close all
set(groot,'defaultAxesFontSize',18, 'defaultAxesTickLabelInterpreter','latex','defaultLegendInterpreter','latex','defaultTextInterpreter','latex');

fw_ang_vect=linspace(0,90,1000);
for j=1:1000
% initial position
r=p.ICs.r0;

% sun direction in body frame
s_n=-r/norm(r);
A_pn=p.ICs.A_np0';
A_bp=eye(3);
A_bn=A_bp*A_pn;
S=A_bn*s_n;

% solar pressure @ 1 AU
P=p.P0/((norm(r)/p.AU)^2);

% offset angle
d_offset=acos(dot(p.ICs.alpha0,s_n));
fw_angle=fw_ang_vect(j);
d1_flywheel=d_offset+fw_angle*pi/180;
d2_flywheel=d_offset-fw_angle*pi/180;
d1_ssp=d_offset;
d2_ssp=d_offset+fw_angle*pi/180;

% retrieve geometry data
A_vec=p.sat.A;
r_mat=p.sat.r;
N_mat_flywheel=p.sat.N(d1_flywheel,d2_flywheel);
N_mat_ssp=p.sat.N(d1_ssp,d2_ssp);
rho_diff=p.sat.rho_diff;
rho_spec=p.sat.rho_spec;

% number of surfaces
n=length(A_vec);   

% init
T_flywheel=zeros(3,1);
T_ssp=zeros(3,1);

for i=1:n
    N=N_mat_flywheel(:,i);
    SN=dot(S,N);
    if SN>0
    A=A_vec(i);
    r=r_mat(:,i);
    rs=rho_spec(i);
    rd=rho_diff(i);
        F=-P*A*(SN)*((1-rs)*S+(2*rs*(SN)+2/3*rd)*N);
        T_temp=cross(r,F);
    else
        T_temp=[0; 0; 0];
    end
    T_flywheel=T_flywheel+T_temp;
end
T_flywheel_mat(:,j)=abs(T_flywheel);

for i=1:n
    N=N_mat_ssp(:,i);
    SN=dot(S,N);
    if SN>0
    A=A_vec(i);
    r=r_mat(:,i);
    rs=rho_spec(i);
    rd=rho_diff(i);
        F=-P*A*(SN)*((1-rs)*S+(2*rs*(SN)+2/3*rd)*N);
        T_temp=cross(r,F);
    else
        T_temp=[0; 0; 0];
    end
    T_ssp=T_ssp+T_temp;
end
T_ssp_mat(:,j)=abs(T_ssp);

T_fw_test(j)=norm(T_flywheel_mat(:,j));
cosangtest(j)=abs(dot(T_flywheel_mat(:,j),cross([0; 1; 0],S)));
T_ssp_test(j)=norm(T_ssp_mat(:,j));
end

figure()
hold on
grid on
% plot(fw_ang_vect,T_ssp_test*1e3);
plot(fw_ang_vect,T_fw_test*1e3);
xlabel('$\Delta$ [$^\circ$]')
ylabel('$T_{SRP}$ [mNms]')
legend('SSP: $\Delta_{rel_1}=0^\circ$, $\Delta_{rel_2}=\Delta$','FW: $\Delta_{rel_1}=\Delta$, $\Delta_{rel_2}=-\Delta$')
legend('$\Delta_{rel_1}=+\Delta$ $\Delta_{rel_2}=-\Delta$')
% [maxim,index]=max(T_test)
% angle=fw_ang_vect(index)

figure()
hold on
grid on
plot(fw_ang_vect,cosangtest);