function satellite_3d_picture(yaw_deg, pitch_deg,roll_deg,...               
d_offset,...
arrays_deflection_deg,...
gimbal_deflection_deg,...
a,...
s,...
edge_color_body,edge_color_gimbal,flag)

model_mat_file='margo_model_new.mat';  
set(groot,'defaultAxesFontSize',12, 'defaultAxesTickLabelInterpreter','latex','defaultLegendInterpreter','latex','defaultTextInterpreter','latex');
set(0, 'DefaultLineLineWidth', 2);

if flag.close==1
options.view_az_el=[10, 290];    
CamUpVec=[0,1,0];
CamPos=[0,0,-3000];
CamTar=[0,0,0];
CamViewAng=10;
else
options.view_az_el=[90, -45];
CamUpVec=[-0.25,sqrt(1-2*0.25^2),0.25];
CamPos=[18000 11000 -18000];
CamTar=[0,0,0];
CamViewAng=9;
end
%%
load(model_mat_file, 'Model3D');

% Get maximum dimension including all the sat parts
AC_DIMENSION = max(max(sqrt(sum(Model3D.Aircraft(1).stl_data.vertices.^2, 2))));
for i=1:length(Model3D.Control)
    AC_DIMENSION = max(AC_DIMENSION, max(max(sqrt(sum(Model3D.Control(i).stl_data.vertices.^2, 2)))));
end

d1_deg=arrays_deflection_deg(:,1);
d2_deg=arrays_deflection_deg(:,2);
th1_deg=gimbal_deflection_deg(:,1);
th2_deg=gimbal_deflection_deg(:,2);

% Initialize the figure

R = AC_DIMENSION;


hf=figure;
%plot sun
if flag.sun==1
plotPlanet(10,[0,0,0],hf,5e-4)
camlight HEADLIGHT;
view(45,30)
quiver3(0,0,0,R,0,0, 'r');
quiver3(0,0,0,0,R,0, 'r');
quiver3(0,0,0,0,0,R, 'r');
text([ 0,R], [ 0, 0], [0,0],'X','Color',[1 0 0],'Fontsize',14);
text([ 0,0], [ 0, R], [0,0],'Y','Color',[1 0 0],'Fontsize',14);
text([ 0,0], [ 0, 0], [0,R],'Z','Color',[1 0 0],'Fontsize',14);
end

AX = axes('position',[0.0 0.0 1 1]);
axis off
scrsz = get(0, 'ScreenSize');
set(hf, 'WindowState','Maximized');
set(AX, 'color', 'none');
axis('equal')
hold on;
cameratoolbar('Show')

% Initializate transformation group handles
% -------------------------------------------------------------------------
% Aircraft transformation group handle
AV_hg         = hgtransform;
% controls_deflection_deg transformation group handles
CONT_hg       = zeros(1,length(Model3D.Control));
for i=1:length(Model3D.Control)
    CONT_hg(i) = hgtransform('Parent', AV_hg, 'tag', Model3D.Control(i).label);
end
% Circles around the aircraft transformation group handles
euler_hgt(1)  = hgtransform('Parent',           AX, 'tag', 'OriginAxes');
euler_hgt(2)  = hgtransform('Parent', euler_hgt(1), 'tag', 'x');
euler_hgt(3)  = hgtransform('Parent', euler_hgt(1), 'tag', 'y');
euler_hgt(4)  = hgtransform('Parent', euler_hgt(1), 'tag', 'z');

euler_hgt(5)  = hgtransform('Parent', euler_hgt(1), 'tag', 'roll_disc');
euler_hgt(6)  = hgtransform('Parent', euler_hgt(1), 'tag', 'pitch_disc');
euler_hgt(7)  = hgtransform('Parent', euler_hgt(1), 'tag', 'heading_disc');
euler_hgt(8)  = hgtransform('Parent', euler_hgt(5), 'tag', 'roll_line');
euler_hgt(9)  = hgtransform('Parent', euler_hgt(6), 'tag', 'pitch_line');
euler_hgt(10)  = hgtransform('Parent', euler_hgt(7), 'tag', 'heading_line');

%%

% Plot airframe
AV = zeros(1, length(Model3D.Aircraft));
for i = 1:length(Model3D.Aircraft)
    AV(i) = patch(Model3D.Aircraft(i).stl_data,  ...
        'FaceColor',        Model3D.Aircraft(i).color, ...
        'EdgeColor',        edge_color_body,        ...
        'FaceLighting',     'gouraud',     ...
        'AmbientStrength',   0.15,          ...
        'LineSmoothing',    'off',...
        'Parent',            AV_hg, ...
        'LineSmoothing', 'off');
end
CONT = zeros(1, (length(Model3D.Control)));
% Plot controls_deflection_deg
for i=1:length(Model3D.Control)
    CONT(i) = patch(Model3D.Control(i).stl_data,  ...
        'FaceColor',        Model3D.Control(i).color, ...
        'EdgeColor',        edge_color_gimbal,        ...
        'FaceLighting',     'gouraud',     ...
        'AmbientStrength',  0.15,          ...
        'LineSmoothing', 'off',...
        'Parent',           CONT_hg(i));
end
% Fixing the axes scaling and setting a nice view angle
% axis('equal');
% axis([-1, 1, -1, 1, -1, 1] * 2.0 * AC_DIMENSION)
set(gcf, 'Color', [1, 1, 1])
% axis off
% zoom(zoom_fact);
AX.View=options.view_az_el;
AX.CameraUpVector=CamUpVec;
AX.CameraViewAngle=CamViewAng;
AX.CameraPosition=CamPos;
AX.CameraTarget=CamTar;

if flag.light==1
% Add a camera light, and tone down the specular highlighting
camlight HEADLIGHT);
material('dull');
end
%% Plot Euler angles references 
if flag.circles==1
% Plot outer circles
phi = (-pi:pi/36:pi)';
D1 = [sin(phi) cos(phi) zeros(size(phi))];
plot3(R * D1(:,1), R * D1(:,2), R * D1(:,3), 'Color', 'b', 'tag', 'Zplane', 'Parent', euler_hgt(7));
plot3(R * D1(:,2), R * D1(:,3), R * D1(:,1), 'Color', [0, 0.8, 0], 'tag', 'Yplane', 'Parent', euler_hgt(6));
plot3(R * D1(:,3), R * D1(:,1), R * D1(:,2), 'Color', 'r', 'tag', 'Xplane', 'Parent', euler_hgt(5));

% Plot +0,+90,+180,+270 Marks
S = 0.95;
phi = -pi+pi/2:pi/2:pi;
D1 = [sin(phi); cos(phi); zeros(size(phi))];

plot3([S * R * D1(1, :); R * D1(1, :)],[S * R * D1(2, :); R * D1(2, :)],[S * R * D1(3, :); R * D1(3, :)], 'Color', 'b', 'tag', 'Zplane', 'Parent',euler_hgt(7));
plot3([S * R * D1(2, :); R * D1(2, :)],[S * R * D1(3, :); R * D1(3, :)],[S * R * D1(1, :); R * D1(1, :)], 'Color',[0 0.8 0], 'tag', 'Yplane', 'Parent',euler_hgt(6));
plot3([S * R * D1(3, :); R * D1(3, :)],[S * R * D1(1, :); R * D1(1, :)],[S * R * D1(2, :); R * D1(2, :)], 'Color', 'r', 'tag', 'Xplane', 'Parent',euler_hgt(5));
text(R*1.1*[1;0;0],R*1.1*[0;1;0],R*1.1*[0;0;1],{'$\delta$','$\beta$','$\alpha$'},'Fontsize',18,'fontweight','bold','color',[0 0 0],'HorizontalAlign','center','VerticalAlign','middle');

% Plot +45,+135,+180,+225,+315 Marks
S = 0.95;
phi = -pi+pi/4:2*pi/4:pi;
D1 = [sin(phi); cos(phi); zeros(size(phi))];
plot3([S*R * D1(1, :); R * D1(1, :)],[S*R * D1(2, :); R * D1(2, :)],[S*R * D1(3, :); R * D1(3, :)], 'Color', 'b', 'tag', 'Zplane', 'Parent',euler_hgt(7));

% 10 deg sub-division marks
S = 0.98;
phi = -180:10:180;
phi = phi*pi / 180;
D1 = [sin(phi); cos(phi); zeros(size(phi))];
plot3([S * R * D1(1, :); R * D1(1, :)],[S * R * D1(2, :); R * D1(2, :)],[S * R * D1(3, :); R * D1(3, :)], 'Color', 'b', 'tag', 'Zplane', 'Parent', euler_hgt(7));
plot3([S * R * D1(2, :); R * D1(2, :)],[S * R * D1(3, :); R * D1(3, :)],[S * R * D1(1, :); R * D1(1, :)], 'Color', [0 0.8 0], 'tag', 'Yplane', 'Parent', euler_hgt(6));
plot3([S * R * D1(3, :); R * D1(3, :)],[S * R * D1(1, :); R * D1(1, :)],[S * R * D1(2, :); R * D1(2, :)], 'Color', 'r', 'tag', 'Xplane', 'Parent', euler_hgt(5));

% Guide lines
plot3([ 0,R], [ 0, 0], [0,0], 'k-');
plot3([ 0,0], [ 0, R], [0,0], 'k-');
plot3([ 0,0], [ 0, 0], [0,R], 'k-');
plot3([-R, R], [ 0, 0], [0, 0], 'b-', 'tag', 'heading_line', 'parent', euler_hgt(10));
plot3([-R, R], [ 0, 0], [0 ,0], 'g-', 'tag',   'pitch_line', 'parent', euler_hgt(9), 'color',[0 0.8 0]);
plot3([ 0, 0], [-R, R], [0, 0], 'r-', 'tag',    'roll_line', 'parent', euler_hgt(8));
end
R2=0.7*R;
k=1.05;
c=0;
lw=2;
% frames
if flag.close==1
 c=-220;  
 k=0.5e-1;
end
if flag.circles~=1
    if flag.pointing_frame==1
quiver3(c,c,c, k*R,0,0, 'k-','LineWidth',1);
quiver3(c,c,c, 0,k*R,0, 'k-','LineWidth',1);
quiver3(c,c,c, 0,0,k*R, 'k-','LineWidth',1);
text(c+R*k*1.05,c,c, '$\delta$','Fontsize',14);
text(c,c+R*k*1.05,c, '$\beta$','Fontsize',14);
text(c,c,c+R*k*1.05, '$\alpha$','Fontsize',14);
    end
quiver3(0,0,0, R2, 0, 0, 'b-','LineWidth',1, 'tag', 'x', 'parent', euler_hgt(2),'LineWidth',lw);
quiver3(0,0,0, 0, R2, 0, 'b-','LineWidth',1, 'tag', 'y', 'parent', euler_hgt(3),'LineWidth',lw);
quiver3(0,0,0, 0, 0, R2, 'b-','LineWidth',1, 'tag', 'z', 'parent', euler_hgt(4),'LineWidth',lw);
text(R2*1.05,0,0,'x','Fontsize',14,'Color',[0 0 1],'HorizontalAlign','center','VerticalAlign','middle','parent', euler_hgt(2));
text(0,R2*1.05,0,'y','Fontsize',14,'Color',[0 0 1],'HorizontalAlign','center','VerticalAlign','middle','parent', euler_hgt(3));
text(0,0,R2*1.05,'z','Fontsize',14,'Color',[0 0 1],'HorizontalAlign','center','VerticalAlign','middle','parent', euler_hgt(4));
end
if flag.sundir==1
%sundir & pointing vector
quiver3(0,0,0, R/2*s(1),R/2*s(2),R/2*s(3), 'Color',[1 0.5 0],'LineWidth',3,'MaxHeadSize',1);
text(R/2*1.05*s(1),R/2*1.05*s(2),R/2*1.05*s(3),'$\hat{S}$','fontweight','bold','Fontsize',14,'Color',[1 0.5 0],'HorizontalAlign','center','VerticalAlign','middle');
end
if flag.point==1
quiver3(0,0,0, R/2*a(1),R/2*a(2),R/2*a(3), 'Color',[0 1 0],'LineWidth',3,'MaxHeadSize',1);
text(R/2*1.05*a(1),R/2*1.05*a(2),R/2*1.05*a(3),'$\hat{\alpha}$','fontweight','bold','Fontsize',14,'Color',[0 1 0],'HorizontalAlign','center','VerticalAlign','middle');
end
%% Rotation
max_deflection = reshape([Model3D.Control(:).max_deflection], 2, length(Model3D.Control(:)));
   
i=1;
    
M1 = makehgtform('zrotate',  yaw_deg(i) * pi / 180);  % Heading rotation
M2 = makehgtform('yrotate', pitch_deg(i) * pi / 180);  % Pitch rotation
M3 = makehgtform('xrotate', roll_deg(i) * pi / 180);  % bank_deg rotation

set(euler_hgt(1), 'Matrix',M1 * M2 * M3)
   set(AV_hg, 'Matrix',M1 * M2 * M3)
 
    

% controls_deflection_deg
for j=1:length(Model3D.Control)
if j<3
    M1 = makehgtform('translate', -Model3D.Control(j).rot_point);   
    M2 = makehgtform('axisrotate', Model3D.Control(j).rot_vect, arrays_deflection_deg(i, j) * pi / 180);  % Pitch
    M3 = makehgtform('translate', Model3D.Control(j).rot_point);  
    set(CONT_hg(j), 'Matrix', M3 * M2 * M1);
else
    % gimbal
    M1 = makehgtform('translate', -Model3D.Control(j).rot_point);   
    M2 = makehgtform('axisrotate', [0,1,0], gimbal_deflection_deg(i, j-2) * pi / 180);  
    M3 = makehgtform('axisrotate', [-1,0,0], gimbal_deflection_deg(i, j-1) * pi / 180);  
    M4 = makehgtform('translate', Model3D.Control(j).rot_point);  
    set(CONT_hg(j), 'Matrix', M4 * M3 * M2 * M1);

end
end

% Detect control surfaces saturations
idx_sat = arrays_deflection_deg(i, :)-d_offset(i) >= max_deflection(2, 1:2)*0.99 | arrays_deflection_deg(i, :)-d_offset(i)  <= max_deflection(1,1:2)*0.99;
idx_nosat = ~idx_sat;
set(CONT(idx_nosat), 'FaceColor', 'y');
set(CONT(idx_sat), 'FaceColor', Model3D.Control(1).color);
