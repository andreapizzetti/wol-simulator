function p=satellite(p,flag)

%% GEOMETRY & OPTICAL PROPERTIES

% main body 250x250x366mm 6U-XL
a=0.25;
b=0.25;
c=0.366;

% solar arrays (2 wings, 4 6U-XL panels each one)
l_sa=1.306;
h_sa=0.209;
l_h=0.1633;  %hinge

% normals: body + SA
N_b=@(d1,d2) [1           0           0;
              0           1           0;
              0           0           1;
             -1           0           0;
              0          -1           0;
              0           0          -1;
              sin(d1)     0           cos(d1);
             -sin(d1)     0          -cos(d1);
              sin(d2)     0           cos(d2);
             -sin(d2)     0          -cos(d2)]';

% Areas: main body + SA
A_v= [b*c a*c a*b b*c a*c a*b h_sa*l_sa h_sa*l_sa h_sa*l_sa h_sa*l_sa];

% Distances
r_mat=[a/2     0                  0;
       0        b/2               0;
       0        0                 c/2;
      -a/2      0                 0;
       0       -b/2               0;
       0        0                -c/2;
       0        l_h+(b+l_sa)/2      0;
       0        l_h+(b+l_sa)/2      0;
       0       -l_h-(b+l_sa)/2      0;
       0       -l_h-(b+l_sa)/2      0]'+p.COGdisp;
       
%% OPTICAL PROPERTIES

% from https://www.hindawi.com/journals/ijae/2015/928206/
% Component MLI SPF WP KV PA RAD BP
% 𝛼BOL      0.42 0.92 0.24 0.73 0.12 0.08 0.97
% 𝛼EOL      0.5 0.92 0.30 0.73 0.15 0.18 0.97
% 𝛾𝑆,BOL    0.29 0.0727 0.38 0.2455 0.8 0.46 0.015
% 𝛾𝑆,EOL    0.071 0.05 0.099 0.168 0.53 0.116 0.004
% 𝛾𝐷,BOL    0.29 0.007 0.38 0.025 0.08 0.46 0.015
% 𝛾𝐷,EOL    0.429 0.030 0.601 0.102 0.3204 0.704 0.0257

opt_prop=[0.42 0.92 0.24 0.73 0.12 0.08 0.97;
0.5 0.92 0.30 0.73 0.15 0.18 0.97;
0.29 0.0727 0.38 0.2455 0.8 0.46 0.015;
0.071 0.05 0.099 0.168 0.53 0.116 0.004;
0.29 0.007 0.38 0.025 0.08 0.46 0.015;
0.429 0.030 0.601 0.102 0.3204 0.704 0.0257];

% assuming polished aluminum (5) for body faces and 2 for solar panels
rho_spec_body=opt_prop(3,5);
rho_diff_body=opt_prop(5,5);
rho_spec_sp=opt_prop(3,2);
rho_diff_sp=opt_prop(5,2);

rho_spec=[rho_spec_body*ones(1,6) rho_spec_sp*ones(1,4)];
rho_diff=[rho_diff_body*ones(1,6) rho_diff_sp*ones(1,4)];

%% CON(STRUCT)
sat.a=a;
sat.b=b;
sat.c=c;
sat.h_sa=h_sa;
sat.l_sa=l_sa;
sat.l_h=l_h;
sat.N=N_b;
sat.A=A_v;
sat.r=r_mat;
sat.rho_diff=rho_diff;
sat.rho_spec=rho_spec;

%% MASS
% from ESA CubeSat Days 05.21
sat.m_wet_tot= 27.5;
sat.m_dry_tot=sat.m_wet_tot-p.ref.mp_tot;

% from GomSpace MSP-B-8-2 DataSheet 
% 326.5 x 205 x 3 mm solar panel with 453 g mass
% M-Argo has 4 8-cell solar panels on each solar array
sat.m_sa= 0.453*4;

sat.m_wet=sat.m_wet_tot-2*sat.m_sa;
sat.m_dry=sat.m_dry_tot-2*sat.m_sa;

%% INERTIA
mp_consumed=(p.ref.starting_hour/p.ref.tof_tot)*p.ref.mp_tot;
m=sat.m_wet_tot - mp_consumed;
[sat.J_nominal, sat.Jinv_nominal]= inertia(0, 0, m, sat);

%% THRUSTER
sat.l_b=[0;
         0;
        -c/2]+p.COGdisp; %thruster is located along -z at half distance

%% REACTION WHEELS
        
[sat.R_RW, sat.Rstar_RW] = rwparam(p,flag);

sat.hsat=19e-3; % from datasheet
sat.Tsat=2e-3;  % from datasheet
   
sat.nRW=size(sat.R_RW,2);

%% RCS
% 12 RCS
% Thrust= 1 mN
% Isp= 16 s
% 4 triplets in the edges, each triplet aligned with the body axis.

% orientation of the forces
f=[ 1  0  0;
    0  1  0;
    0  0  1;
    1  0  0;
    0 -1  0;
    0  0 -1;
   -1  0  0;
    0 -1  0;
    0  0  1;
   -1  0  0;
    0  1  0;
    0  0 -1];
    
r=[ b/2  b/2  c/2;
    b/2  b/2  c/2;
    b/2  b/2  c/2;
    b/2 -b/2 -c/2;
    b/2 -b/2 -c/2;
    b/2 -b/2 -c/2;
   -b/2 -b/2  c/2;
   -b/2 -b/2  c/2;
   -b/2 -b/2  c/2;
   -b/2  b/2 -c/2;
   -b/2  b/2 -c/2;
   -b/2  b/2 -c/2];

R_RCS=cross(r,f)';

Rstar_RCS=R_RCS'/(R_RCS*R_RCS');

sat.R_RCS=R_RCS;
sat.Rstar_RCS=Rstar_RCS;

sat.Isp_RCS=16;

%% (CON)STRUCT
p.sat=sat;
end