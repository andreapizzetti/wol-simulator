function [p, flag]=initial(p,flag)

%% INITIAL CONDITIONS

% mass
mp_consumed=(p.ref.starting_hour/p.ref.tof_tot)*p.ref.mp_tot;
p.ICs.m0=p.sat.m_wet_tot - mp_consumed;

% angular momentum    
p.ICs.h0=p.ICs.h0*1e-3;

% angular velocity
p.ICs.w0=[0; 0; 0];

% Position
p.ICs.r0=p.ref.r(:,1);

% Velocity
p.ICs.v0=p.ref.v(:,1);

% Pointing Vector
p.ICs.alpha0=p.ref.alpha(:,1);
    
% Pointing Frame wrt Inertial Frame
alpha=p.ICs.alpha0;
beta=cross(p.ICs.r0,alpha)/norm(cross(p.ICs.r0,alpha));
delta=cross(beta,alpha)/norm(cross(beta,alpha));
p.ICs.A_np0=[delta...
             beta,...
             alpha];   
          % satellite with z axis aligned with 
          % pointing vector and y axis
          % perpendicular to sun direction

% Body Frame wrt Pointing Frame
[p,flag]=guidance_logic(p,flag);

% Body Frame wrt Inertial Frame
p.ICs.A_bn0=p.ICs.A_pb0'*p.ICs.A_np0';
         
end
