%% CONTROLLER
function p=controller(p,flag)

%% LQR GAIN
I=p.sat.J_nominal;

I_x=I(1,1);
I_y=I(2,2);
I_z=I(3,3);

% OPEN-LOOP SYSTEM
A=[zeros(3) eye(3);
   zeros(3) zeros(3)];
B=[0     0     0;
   0     0     0;
   0     0     0;
   1/I_x 0     0;
   0     1/I_y 0;
   0     0     1/I_z];
C=[eye(3) zeros(3)];

% LQR 
if flag.cruising==1
Wzz=[1    0    0;
     0    1    0;
     0    0    1]; %the higher the value, the lower must be the performance
% Wzz=p.sat.J_nominal/det(p.sat.J_nominal);
Wuu=1e5*Wzz; %the lower the value, the higher can be the control 
else
Wzz=[1     0   0;
     0     1   0;
     0     0   1];
Wuu=1e8*Wzz;
end
 
Q=C'*Wzz*C;
R=Wuu;

[p.K_LQR,~,~]=lqr(A,B,Q,R,0);

%% TRACKING GAINS
    
if flag.cruising==1  
    K_a=5e-3;
    K_w=5e-1;  
else
    K_a=5e-5;
    K_w=5e-2;
end

p.K=[diag(ones(3,1)*K_a), diag(ones(3,1)*K_w)];

%% WOL SINGULARITY GAIN
% Final WOL in 4 RWs case
p.K_wol=-1e-4;
end