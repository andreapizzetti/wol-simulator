function p=reference(p,flag)

% CHOOSE TRAJECTORY
data_csv=readtable('Baseline_2010UE51_states_control.csv');
% data_csv=readtable('Backup_2000SG344_states_control.csv');
% data_csv=readtable('2014YD_05Jun2024_900_control');
tfuelopt=data_csv{:,1};
fuelopt=data_csv{:,2:end};

% NEA                     DEP.TIME   DEP.DATE       TOF  PROP
% 2000 SG344 Baseline     8712       08-Nov-2023    833  1.156
% Backup                  9077       07-Nov-2024    707  1.051
% 2010 UE51 Baseline      8660       17-Sep-2023    581  0.974
% Backup                  9025       16-Sep-2024    861  1.787
% 2011MD Baseline         8556       05-Jun-2023    854  1.536
% Backup                  8869       13-Apr-2024    742  1.386
% 2012 UV136 Baseline     8712       08-Nov-2023    805  1.895
% Backup                  9077       07-Nov-2024    861  2.313
% 2014 YD Baseline        8608       27-Jul-2023    721  1.966
% Backup                  8973       26-Jul-2024    651  1.518

tof_tot_days=581;
mp=0.974;

ref.tof_tot=tof_tot_days*24+2*tof_tot_days/7; %total hours of the NEA trajectory in hours
ref.mp_tot=mp;   %propellant mass used in kg

%% SELECTION OF REFERENCE CRUISING AND COASTING ARCS
% let's consider the 6-days cruising arc of 4-10 february 2024 12:01:09
% where we have the lowest difference value between maximum thrust 
% and actual thrust
range_cruising=3401:3545;
% duty cycle 10 february 2024 12:01:09 - 11 february 2024 12:01:09
range_coasting=range_cruising(1:25)+145;

if flag.cruising==1
range=range_cruising;
elseif flag.cruising==0
range=range_coasting;
% range=7116:7140;
end

ref.time=tfuelopt(range);
ref.pow=fuelopt(range,1);
ref.thr=fuelopt(range,2);
ref.r=fuelopt(range,5:7)';
ref.v=fuelopt(range,8:10)';
ref.starting_hour=range(1); %starting hour

%% POINTING VECTOR & THRUST VECTOR

% init
point=zeros(3,length(range));

if flag.cruising==1
alpha=fuelopt(range,3);
beta=fuelopt(range,4);

a=deg2rad(alpha);
b=deg2rad(beta);

for i=1:length(range)
    v=ref.v(:,i);
    r=ref.r(:,i);
    h=cross(r,v)/norm(cross(r,v));
    T_nvh=[v/norm(v),cross(h,v)/norm(cross(h,v)), h];
    % components in VH frame (v-cross(h,v)-h)    
    p_VH(:,i)=[cos(b(i))*cos(a(i));
               cos(b(i))*sin(a(i));
               sin(b(i))];
    % components in inertial frame (X-Y-Z)
     point(:,i)=T_nvh*p_VH(:,i);

% point(:,i)=[cos(b(i))*sin(a(i));
%             cos(b(i))*cos(a(i));
%             sin(b(i))];
end
ref.alpha=point;

elseif flag.cruising==0
    % during coasting, alpha and beta are set to 0. We use as poointing
    % vector the last pointing vector of the previous cruising arc.
alpha=fuelopt(range_cruising(end),3);
beta=fuelopt(range_cruising(end),4);

a=deg2rad(alpha);
b=deg2rad(beta);

v=fuelopt(range_cruising(end),8:10)';
r=fuelopt(range_cruising(end),5:7)';
h=cross(r,v)/norm(cross(r,v));
T_nvh=[v/norm(v),cross(h,v)/norm(cross(h,v)), h];
% components in VH frame (v-cross(h,v)-h)    
p_VH=[cos(b)*cos(a);
      cos(b)*sin(a);
      sin(b)];
% components in inertial frame (X-Y-Z)
point=T_nvh*p_VH;

% point=[cos(b)*sin(a);
%        cos(b)*cos(a);
%        sin(b)];
%         
ref.alpha=repmat(point,1,length(range));
end

%% TESTING PURPOSES
% for testing purposes let's consider X axis as position vector and Z axis
% as pointing vector

if flag.debug==1
    r=norm(ref.r(:,1))*[1;0;0];
    v=norm(ref.v(:,1))*[0;1;0];
    alpha=[0;0;1];
    tau=norm(ref.tau(:,1))*alpha;
    
    ref.r=repmat(r',length(range),1)';
    ref.v=repmat(v',length(range),1)';
    ref.alpha=repmat(alpha',length(range),1)';
    ref.tau=repmat(tau',length(range),1)';
end

%% CONSTRUCTING EPHEMERIS
ref.eph.time=3600*(0:1:length(range)-1)';    %s
ref.eph.signals.values= [ref.r; ref.alpha; ref.thr']'; %25*7

% %% EXTRACTING POLYNOMIAL COEFFICIENTS
ref.hours=0:1:length(range)-1;

% extract fitting polynomial coefficients for the desired thrust vector
ref.tau=ref.thr'.*point;
tau_coeffx=polyfit(ref.hours,ref.tau(1,:),2);
tau_coeffy=polyfit(ref.hours,ref.tau(2,:),2);
tau_coeffz=polyfit(ref.hours,ref.tau(3,:),2);
ref.tau_coeff=[tau_coeffx; tau_coeffy; tau_coeffz];

% extract fitting polynomial coefficients for the desired position vector
r_coeffx=polyfit(ref.hours,ref.r(1,:),2);
r_coeffy=polyfit(ref.hours,ref.r(2,:),2);
r_coeffz=polyfit(ref.hours,ref.r(3,:),2);
ref.r_coeff=[r_coeffx; r_coeffy; r_coeffz];

p.ref=ref;