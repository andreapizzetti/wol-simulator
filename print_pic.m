close all
addpath thesis_plots animate

flag.sun=0;
flag.light=1;
flag.point=0;
flag.sundir=0;
flag.close=0;
flag.circles=0;
flag.pointing_frame=0;

yaw_deg=0;           
pitch_deg=0;              
roll_deg=0;               
d_offset=2.618902790582709*180/pi;
d_offset=180;
arrays_deflection_deg=[0,0]+d_offset;
gimbal_deflection_deg=[0,0];
a=[0 0 1];
s=[0.499212652184057,0,-0.866479502296136]; 
s=[0 0 -1];

edge_color_body='None';
edge_color_gimbal='None';

satellite_3d_picture(yaw_deg, pitch_deg,roll_deg,...               
d_offset,...
arrays_deflection_deg,...
gimbal_deflection_deg,...
a,...
s,...
edge_color_body,edge_color_gimbal,flag)

%%
f=figure(11);
square=0;
pap='pdf';
title='cruising_4_m';

if square==1
f.Position=[1,41,900,900];
f.PaperSize=[16 16];
else
f.Position=[1,41,1800,900];
%f.PaperSize=[30 15];
f.PaperSize=[24 12];
end

if strcmp(pap,'pdf')
print(f,title,'-dpdf','-fillpage','-r600')
else
    
print(f,title,'-dpng','-r600')
end
