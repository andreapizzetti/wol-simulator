function animate(out,flag,movie)
speed=movie.speed;
title=movie.title;

% Load the 3D model
model_info_file='margo_model_new.mat';
% Load the simulation data
d1=out.d1;
d2=out.d2;
d1_rel=out.d1_rel;
d2_rel=out.d2_rel;
th1=out.th1;
th2=out.th2;
yaw=out.yaw;
pitch=out.pitch;
roll=out.roll;
angmom=out.h;
sundir=1000*out.S_p;
state=out.state;
% define the reproduction speed factor
speedx = speed*30*out.tout(end)/3600; 
% Do you want to save the animation in a mp4 file? (0.No, 1.Yes)
% flag_movie = 0;
% Movie file name
movie_file_name = title;
% camera options
options.view_az_el=[116, -40];
options.CameraPosition=[18736 9153 -18462];
options.CameraUpVector=[-0.226,0.944,0.2386];
options.CameraViewAngle=7.33;

%zoom
zoom_fact=2;

% -------------------------------------------------------------------------
% The frame sample time shall be higher than 0.02 seconds to be able to 
% update the figure (CPU/GPU constraints)
frame_sample_time = max(0.02, out.tout(2)-out.tout(1));
frame_sample_time=0.05;
% Resample the time vector to modify the reproduction speed
t_new   = out.tout(1):frame_sample_time*(speedx):out.tout(end);
% Resample the recorded data
d1_deg     = interp1(out.tout, d1, t_new','linear')*180/pi;
d2_deg     = interp1(out.tout, d2, t_new','linear')*180/pi;
d1_rel_deg     = interp1(out.tout, d1_rel, t_new','linear')*180/pi;
d2_rel_deg     = interp1(out.tout, d2_rel, t_new','linear')*180/pi;
th1_deg     = interp1(out.tout, th1, t_new','linear')*180/pi;
th2_deg     = interp1(out.tout, th2, t_new','linear')*180/pi;

% We have to be careful with angles with ranges
yaw_deg  = atan2(interp1(out.tout, sin(yaw), t_new','linear'), interp1(out.tout, cos(yaw), t_new','linear')) * 180 / pi;
pitch_deg  = atan2(interp1(out.tout, sin(pitch), t_new','linear'), interp1(out.tout, cos(pitch), t_new','linear')) * 180 / pi;
roll_deg  = atan2(interp1(out.tout, sin(roll), t_new','linear'), interp1(out.tout, cos(roll), t_new','linear')) * 180 / pi;

% also angular momentum
angmom = interp1(out.tout,angmom,t_new','linear')/1e-3;

% also sun direction
sundir = interp1(out.tout,sundir,t_new','linear');

% also state
state= interp1(out.tout, state, t_new', 'linear');

% Control array assignation
arrays_deflection_deg = [d1_deg(:),d2_deg(:)];
arrays_deflection_rel_deg = [d1_rel_deg(:),d2_rel_deg(:)];
gimbal_deflection_deg = [th1_deg(:),th2_deg(:)];

%% Run aircraft_3d_animation function
% -------------------------------------------------------------------------
satellite_3d_animation(model_info_file,...,
    t_new,...
    yaw_deg, ...            Yaw angle [deg]
    pitch_deg, ...              Pitch angle [deg]
    roll_deg, ...               Roll angle [deg]
    arrays_deflection_deg, ...
    arrays_deflection_rel_deg,...
    gimbal_deflection_deg,...
    angmom,...
    sundir,...
    state,...
    options,...
    zoom_fact,...
    frame_sample_time, ...      Sample time [sec]
    speedx, ...                 Reproduction speed
    flag, ...            Save the movie? 0-1
    movie_file_name);           % Movie file name

end