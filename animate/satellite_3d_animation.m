function [] = satellite_3d_animation(...
    model_mat_file, ...         
    t_new,...
    yaw_deg, ...            
    pitch_deg, ...              
    roll_deg, ...               
    arrays_deflection_deg, ...
    arrays_deflection_rel_deg, ...
    gimbal_deflection_deg, ...
    angmom,...
    sundir,...
    state,...
    options,...
    zoom_fact,...
    frame_time, ...             
    speedx, ...               
    flag, ...           
    movie_file_name)           

load(model_mat_file, 'Model3D');

% Open the video output if we are recording the movie
if flag.movie == 1
    aviobj = VideoWriter(movie_file_name, 'MPEG-4');
    aviobj.Quality = 100;  % movie quality
    aviobj.FrameRate = 1/frame_time;
    open(aviobj);
end

% Get maximum dimension including all the sat parts
AC_DIMENSION = max(max(sqrt(sum(Model3D.Aircraft(1).stl_data.vertices.^2, 2))));
for i=1:length(Model3D.Control)
    AC_DIMENSION = max(AC_DIMENSION, max(max(sqrt(sum(Model3D.Control(i).stl_data.vertices.^2, 2)))));
end

d1_deg=arrays_deflection_deg(:,1);
d2_deg=arrays_deflection_deg(:,2);
d1_rel_deg=arrays_deflection_rel_deg(:,1);
d2_rel_deg=arrays_deflection_rel_deg(:,2);
th1_deg=gimbal_deflection_deg(:,1);
th2_deg=gimbal_deflection_deg(:,2);

%% Initialize the figure
hf = figure;
AX = axes('position',[0.0 0.0 1 1]);
axis off
set(gcf, 'WindowState','Maximized');
set(AX, 'color', 'none');
axis('equal')
hold on;
cameratoolbar('Show')

% Initializate transformation group handles
% -------------------------------------------------------------------------
% Aircraft transformation group handle
AV_hg         = hgtransform;
% controls_deflection_deg transformation group handles
CONT_hg       = zeros(1,length(Model3D.Control));
for i=1:length(Model3D.Control)
    CONT_hg(i) = hgtransform('Parent', AV_hg, 'tag', Model3D.Control(i).label);
end
% Circles around the aircraft transformation group handles
euler_hgt(1)  = hgtransform('Parent',           AX, 'tag', 'OriginAxes');
euler_hgt(2)  = hgtransform('Parent', euler_hgt(1), 'tag', 'roll_disc');
euler_hgt(3)  = hgtransform('Parent', euler_hgt(1), 'tag', 'pitch_disc');
euler_hgt(4)  = hgtransform('Parent', euler_hgt(1), 'tag', 'heading_disc');
euler_hgt(5)  = hgtransform('Parent', euler_hgt(2), 'tag', 'roll_line');
euler_hgt(6)  = hgtransform('Parent', euler_hgt(3), 'tag', 'pitch_line');
euler_hgt(7)  = hgtransform('Parent', euler_hgt(4), 'tag', 'heading_line');
euler_hgt(8)  = hgtransform('Parent', euler_hgt(2), 'tag', 'thrust_axis');
% Plot objects
sun=quiver3(0,0,0,sundir(1,1),sundir(1,2),sundir(1,3),'color',[0.9 0.6 0],'LineWidth',4);

% Plot airframe
AV = zeros(1, length(Model3D.Aircraft));
for i = 1:length(Model3D.Aircraft)
    AV(i) = patch(Model3D.Aircraft(i).stl_data,  ...
        'FaceColor',        Model3D.Aircraft(i).color, ...
        'EdgeColor',        'none',        ...
        'FaceLighting',     'gouraud',     ...
        'AmbientStrength',   0.15,          ...
        'LineSmoothing',    'off',...
        'Parent',            AV_hg, ...
        'LineSmoothing', 'off');
end
CONT = zeros(1, (length(Model3D.Control)));
% Plot controls_deflection_deg
for i=1:length(Model3D.Control)
    CONT(i) = patch(Model3D.Control(i).stl_data,  ...
        'FaceColor',        Model3D.Control(i).color, ...
        'EdgeColor',        'none',        ...
        'FaceLighting',     'gouraud',     ...
        'AmbientStrength',  0.15,          ...
        'LineSmoothing', 'off',...
        'Parent',           CONT_hg(i));
end
% Fixing the axes scaling and setting a nice view angle
axis('equal');
axis([-1, 1, -1, 1, -1, 1] * 2.0 * AC_DIMENSION)
set(gcf, 'Color', [1, 1, 1])
axis off
zoom(zoom_fact);
AX.View=options.view_az_el;
AX.CameraPosition=options.CameraPosition;
AX.CameraUpVector=options.CameraUpVector;
AX.CameraViewAngle=options.CameraViewAngle;

% Add a camera light, and tone down the specular highlighting
camlight HEADLIGHT);
material('dull');

%% Plot Euler angles references 
R = AC_DIMENSION;

% Plot outer circles
phi = (-pi:pi/36:pi)';
D1 = [sin(phi) cos(phi) zeros(size(phi))];
plot3(R * D1(:,1), R * D1(:,2), R * D1(:,3), 'Color', 'b', 'tag', 'Zplane', 'Parent', euler_hgt(4));
plot3(R * D1(:,2), R * D1(:,3), R * D1(:,1), 'Color', [0, 0.8, 0], 'tag', 'Yplane', 'Parent', euler_hgt(3));
plot3(R * D1(:,3), R * D1(:,1), R * D1(:,2), 'Color', 'r', 'tag', 'Xplane', 'Parent', euler_hgt(2));

% Plot +0,+90,+180,+270 Marks
S = 0.95;
phi = -pi+pi/2:pi/2:pi;
D1 = [sin(phi); cos(phi); zeros(size(phi))];

plot3([S * R * D1(1, :); R * D1(1, :)],[S * R * D1(2, :); R * D1(2, :)],[S * R * D1(3, :); R * D1(3, :)], 'Color', 'b', 'tag', 'Zplane', 'Parent',euler_hgt(4));
plot3([S * R * D1(2, :); R * D1(2, :)],[S * R * D1(3, :); R * D1(3, :)],[S * R * D1(1, :); R * D1(1, :)], 'Color',[0 0.8 0], 'tag', 'Yplane', 'Parent',euler_hgt(3));
plot3([S * R * D1(3, :); R * D1(3, :)],[S * R * D1(1, :); R * D1(1, :)],[S * R * D1(2, :); R * D1(2, :)], 'Color', 'r', 'tag', 'Xplane', 'Parent',euler_hgt(2));
text(R*1.05*[1;0;0],R*1.05*[0;1;0],R*1.05*[0;0;1],{'$\delta$','$\beta$','$\alpha$'},'Fontsize',14,'color',[0 0 0],'HorizontalAlign','center','VerticalAlign','middle');
% text(R*1.05*D2(1,1),R*1.05*D2(2,1),R*1.05*D2(3,1),'-\delta','Fontsize',14,'color',[0 0 0],'HorizontalAlign','center','VerticalAlign','middle');

% Plot +45,+135,+180,+225,+315 Marks
S = 0.95;
phi = -pi+pi/4:2*pi/4:pi;
D1 = [sin(phi); cos(phi); zeros(size(phi))];
plot3([S*R * D1(1, :); R * D1(1, :)],[S*R * D1(2, :); R * D1(2, :)],[S*R * D1(3, :); R * D1(3, :)], 'Color', 'b', 'tag', 'Zplane', 'Parent',euler_hgt(4));

% 10 deg sub-division marks
S = 0.98;
phi = -180:10:180;
phi = phi*pi / 180;
D1 = [sin(phi); cos(phi); zeros(size(phi))];
plot3([S * R * D1(1, :); R * D1(1, :)],[S * R * D1(2, :); R * D1(2, :)],[S * R * D1(3, :); R * D1(3, :)], 'Color', 'b', 'tag', 'Zplane', 'Parent', euler_hgt(4));
plot3([S * R * D1(2, :); R * D1(2, :)],[S * R * D1(3, :); R * D1(3, :)],[S * R * D1(1, :); R * D1(1, :)], 'Color', [0 0.8 0], 'tag', 'Yplane', 'Parent', euler_hgt(3));
plot3([S * R * D1(3, :); R * D1(3, :)],[S * R * D1(1, :); R * D1(1, :)],[S * R * D1(2, :); R * D1(2, :)], 'Color', 'r', 'tag', 'Xplane', 'Parent', euler_hgt(2));

% Guide lines
plot3([ 0,R], [ 0, 0], [0,0], 'k-');
plot3([ 0,0], [ 0, R], [0,0], 'k-');
plot3([ 0,0], [ 0, 0], [0,R], 'k-');
plot3([-R, R], [ 0, 0], [0, 0], 'b-', 'tag', 'heading_line', 'parent', euler_hgt(7));
plot3([-R, R], [ 0, 0], [0 ,0], 'g-', 'tag',   'pitch_line', 'parent', euler_hgt(6), 'color',[0 0.8 0]);
plot3([ 0, 0], [-R, R], [0, 0], 'r-', 'tag',    'roll_line', 'parent', euler_hgt(5));

% thrust axis
% plot3([0, 0], [0, 0], [-0.15 ,-R+0.15], 'tag',   'thruster_axis', 'parent', euler_hgt(8), 'color',[0 1 1],'LineWidth',2);

% Initialize text handles
FontSize    = 12;
text_color  = [0, 0, 0];
font_name   = 'Palatino';
% hdle_text_t                 = text(-0.45 * AC_DIMENSION * 1.5, 0.55 * AC_DIMENSION * 1.5, 't=  0 h', 'Color',text_color, 'FontSize',FontSize, 'FontName', font_name);
% hdle_text_yaw               = text(-0.45 * AC_DIMENSION * 1.5, 0.55 * AC_DIMENSION * 1.5, 0.5 * AC_DIMENSION * 1.5, '', 'Color','r', 'FontSize', FontSize, 'FontName', font_name);
% hdle_text_pitch               = text(-0.45 * AC_DIMENSION * 1.5, 0.55 * AC_DIMENSION * 1.5, 0.4 * AC_DIMENSION * 1.5, '', 'Color','g', 'FontSize', FontSize, 'FontName', font_name);
% hdle_text_roll               = text(-0.45 * AC_DIMENSION * 1.5, 0.55 * AC_DIMENSION * 1.5, 0.3 * AC_DIMENSION * 1.5, '', 'Color','b', 'FontSize', FontSize, 'FontName', font_name);
% hdle_text_state                = text(0 ,0, 1 * AC_DIMENSION * 1.5, 'START', 'Color', [0 0 0], 'FontSize', 15 , 'FontName', font_name);
hdle_text_t                 = text(-1.05 * AC_DIMENSION * 1.5, 0.1* AC_DIMENSION * 1.5, -0.35 * AC_DIMENSION * 1.5, 't=  0 h', 'Color',text_color, 'FontSize',FontSize, 'FontName', font_name);
hdle_text_yaw               = text(-1.05 * AC_DIMENSION * 1.5, 0.25* AC_DIMENSION * 1.5, -0.35 * AC_DIMENSION * 1.5, '', 'Color','r', 'FontSize', FontSize, 'FontName', font_name);
hdle_text_pitch               = text(-1.05 * AC_DIMENSION * 1.5, 0.3* AC_DIMENSION * 1.5, -0.35 * AC_DIMENSION * 1.5, '', 'Color','g', 'FontSize', FontSize, 'FontName', font_name);
hdle_text_roll               = text(-1.05 * AC_DIMENSION * 1.5, 0.35* AC_DIMENSION * 1.5, -0.35 * AC_DIMENSION * 1.5, '', 'Color','b', 'FontSize', FontSize, 'FontName', font_name);

state_name=state_def(state(1), d1_deg(1)-d2_deg(1));
if flag.statemachine==0
    state_name='';
end
hdle_text_state                = text(-1.05 * AC_DIMENSION * 1.5, 0.5* AC_DIMENSION * 1.5, -0.35 * AC_DIMENSION * 1.5, state_name, 'Color', [0 0 0], 'FontSize', 15 , 'FontName', font_name);
state_before=state_name;
counter=0;

% Superpolt a new axes on top of the 3D model
h_sa     = axes('Position',[0.8, 0.1, 0.1, 0.2],'FontSize', FontSize);
hold(h_sa, 'on');
h_sadm = plot(h_sa, d1_rel_deg(1),  d2_rel_deg(1),'o', 'Color', [0, 0, 0], 'XDataSource', 'DELTA1', 'YDataSource', 'DELTA2', 'LineWidth', 1);
grid on
axis([-117, 117, -117, 117])
hold all; box on;
xlabel('$\Delta_{rel_1}$'); ylabel('$\Delta_{rel_2}$')

h_gimb     = axes('Position',[0.8, 0.4, 0.1, 0.2], 'FontSize', FontSize);
hold(h_gimb, 'on');
h_gimbal = plot(h_gimb, th1_deg(1),  th2_deg(1),'o', 'Color', [0, 0, 0], 'XDataSource', 'THETA1', 'YDataSource', 'THETA2', 'LineWidth', 1);
grid on
axis([-15, 15, -15, 15])
hold all; box on;
xlabel('$\theta_1$'); ylabel('$\theta_2$')

h_mom     = axes('Position',[0.05, 0.1, 0.25, 0.4], 'FontSize', FontSize);
n_RW=size(angmom,2);
hold(h_mom, 'on');
plot(h_mom, t_new, angmom);
h_angmom = plot(h_mom, zeros(1,n_RW), angmom(1,:),'ko', 'XDataSource', 'TIME_RW', 'YDataSource', 'ANGMOM', 'LineWidth', 1);
grid on
axis([0, t_new(end), -20, 20])
hold all; box on;
xlabel('time [s]'); ylabel('h [mNms]')
%% Animation Loop

% Refresh plot for flight visualization
tic;
for i=1:length(yaw_deg)
    
    % Pitch disc
    M = makehgtform('zrotate', yaw_deg(i)* pi / 180);      % Heading rotation
    set(euler_hgt(3), 'Matrix', M)
    
    % Roll disc
    M1 = makehgtform('zrotate',  yaw_deg(i) * pi / 180);   % Heading rotation
    M2 = makehgtform('yrotate', pitch_deg(i) * pi / 180);   % Pitch rotation
    set(euler_hgt(2), 'Matrix', M1 * M2)
    
    % Roll line
    M = makehgtform('xrotate', roll_deg(i) * pi / 180);
    set(euler_hgt(5), 'Matrix', M)
    
    % Pitch line
    M = makehgtform('yrotate', pitch_deg(i) * pi / 180);
    set(euler_hgt(6), 'Matrix', M)
    
    % Heading line
    M = makehgtform('zrotate', yaw_deg(i) * pi / 180);
    set(euler_hgt(7), 'Matrix', M)
    
    % AIRCRAFT BODY
    M1 = makehgtform('zrotate',  yaw_deg(i) * pi / 180);  % Heading rotation
    M2 = makehgtform('yrotate', pitch_deg(i) * pi / 180);  % Pitch rotation
    M3 = makehgtform('xrotate', roll_deg(i) * pi / 180);  % bank_deg rotation
    set(AV_hg, 'Matrix',M1 * M2 * M3)
    
    % Thrust axis
    M1 = makehgtform('translate', -[0,0,-0.15]);   
    M2 = makehgtform('axisrotate', [0,1,0], gimbal_deflection_deg(i, 1) * pi / 180);  
    M3 = makehgtform('axisrotate', [-1,0,0], gimbal_deflection_deg(i, 2) * pi / 180);  
    M4 = makehgtform('translate',  +[0,0,-0.15]);  
    set(euler_hgt(8), 'Matrix', M4 * M3 * M2 * M1);
    
    
    % controls_deflection_deg
    for j=1:length(Model3D.Control)
        if j<3
            M1 = makehgtform('translate', -Model3D.Control(j).rot_point);   
            M2 = makehgtform('axisrotate', Model3D.Control(j).rot_vect, arrays_deflection_deg(i, j) * pi / 180);  % Pitch
            M3 = makehgtform('translate', Model3D.Control(j).rot_point);  
            set(CONT_hg(j), 'Matrix', M3 * M2 * M1);
        else
            % gimbal
            M1 = makehgtform('translate', -Model3D.Control(j).rot_point);   
            M2 = makehgtform('axisrotate', [0,1,0], gimbal_deflection_deg(i, j-2) * pi / 180);  
            M3 = makehgtform('axisrotate', [-1,0,0], gimbal_deflection_deg(i, j-1) * pi / 180);  
            M4 = makehgtform('translate', Model3D.Control(j).rot_point);  
            set(CONT_hg(j), 'Matrix', M4 * M3 * M2 * M1);

        end
    end
        
    set(hdle_text_t, 'String',sprintf('t= %3.2f h',(i-1)*frame_time*speedx/3600))
    
    % Detect control surfaces saturations
    idx_sat = [d1_rel_deg(i) d2_rel_deg(i)] >= 90 | [d1_rel_deg(i) d2_rel_deg(i)] <= -90;
    idx_nosat = ~idx_sat;
    set(CONT(idx_nosat), 'FaceColor', 'y');
    set(CONT(idx_sat), 'FaceColor', Model3D.Control(1).color);
 
    DELTA1  = d1_rel_deg(i);
    DELTA2  = d2_rel_deg(i);
    THETA1 = th1_deg(i);
    THETA2 = th2_deg(i);
    
    TIME = (i-1)*frame_time*speedx*ones(1,3);
    TIME_RW = (i-1)*frame_time*speedx*ones(1,n_RW);
    ANGMOM = angmom(i,:);
    
    % Refresh  plots
    refreshdata(h_sadm, 'caller')
    refreshdata(h_gimbal, 'caller')
    refreshdata(h_angmom,'caller')
    
    % in m-argo we have alpha as roll axis
    set(hdle_text_yaw, 'String',strcat('yaw= ',num2str(roll_deg(i), '%2.1f'), ' deg'))
    set(hdle_text_pitch, 'String',strcat('pitch= ',num2str(pitch_deg(i), '%2.1f'), ' deg'))
    set(hdle_text_roll, 'String',strcat('roll= ',num2str(yaw_deg(i), '%2.1f'), ' deg'))

    % indication for SM
    if flag.statemachine==1
    state_name=state_def(state(i), d1_deg(i)-d2_deg(i));
    set(hdle_text_state,'String',state_name);
    counter=counter+1;
    if strcmp(state_before,state_name)~=1
        counter=0;
    set(hdle_text_state,'EdgeColor',[1 0 0]);
    end
    if counter >10 
    set(hdle_text_state,'EdgeColor',[1 1 1]);
    end
    state_before=state_name;
    end
    
    % Sun direction
    set(sun,'udata',sundir(i,1),'vdata',sundir(i,2),'wdata',sundir(i,3));
    
    % Real-time
    drawnow;
    if frame_time * i - toc > 0
        pause(max(0, frame_time * i - toc))
    end
    
    if flag.movie == 1
        writeVideo(aviobj, getframe(hf));
    end
    
    
end
toc
if flag.movie == 1
    close(aviobj);
end
end
