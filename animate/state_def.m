function state_name = state_def(state, d_diff)
state_name='';

switch state

    case -1
        state_name='RE-POINTING';
        
    case 0
        if abs(d_diff) <1
            state_name='GIMBAL';
        end       
        if abs(d_diff) <91 && abs(d_diff) >89
            state_name='SSA';
        end
        if abs(d_diff) <71 && abs(d_diff) >69
            state_name='PW';
        end
    case 1
        state_name='BETA';
        
    case 2
        state_name='SRPW';
        
    case 3
        state_name='WOL SINGULARITY';
        
    case 4
        state_name='DE-TUMBLING';
        
    case 5
        state_name='NO WOL';
end
end

