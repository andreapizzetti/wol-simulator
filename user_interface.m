function varargout = user_interface(varargin)
% USER_INTERFACE MATLAB code for user_interface.fig
%      USER_INTERFACE, by itself, creates a new USER_INTERFACE or raises the existing
%      singleton*.
%
%      H = USER_INTERFACE returns the handle to a new USER_INTERFACE or the handle to
%      the existing singleton*.
%
%      USER_INTERFACE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in USER_INTERFACE.M with the given input arguments.
%
%      USER_INTERFACE('Property','Value',...) creates a new USER_INTERFACE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before user_interface_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to user_interface_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help user_interface

% Last Modified by GUIDE v2.5 24-Aug-2021 10:58:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @user_interface_OpeningFcn, ...
                   'gui_OutputFcn',  @user_interface_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before user_interface is made visible.
function user_interface_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to user_interface (see VARARGIN)

% Choose default command line output for user_interface
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes user_interface wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = user_interface_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in runSim.
function runSim_Callback(hObject, eventdata, handles)
% hObject    handle to runSim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
input.RWnumber = get(handles.RWnumber,'Value');
input.h1= get(handles.RW1,'String');
input.h2= get(handles.RW2,'String');
input.h3= get(handles.RW3,'String');
input.h4= get(handles.RW4,'String');
input.RWdir= get(handles.RWdir,'Value');
input.xCOG= get(handles.xCOG,'String');
input.yCOG= get(handles.yCOG,'String');
input.zCOG= get(handles.zCOG,'String');
input.flagAnim= get(handles.flagAnim,'Value');
input.flagSM= get(handles.flagSM,'Value');
input.flag6DOF= get(handles.flag6DOF,'Value');
input.flagWOL= get(handles.flagWOL,'Value');
input.flagInertia= get(handles.flagInertia,'Value');
input.flagWOLstop=get(handles.flagWOLstop,'Value');
input.simTime= get(handles.simTime,'String');
input.scenario = get(get(handles.scenario,'SelectedObject'),'String');
set(0,'userdata',input);
uicontrol(hObject)   
uiresume;           
close(handles.figure1)   
run_simulation(input)

% --- Executes on button press in stopSim.
function stopSim_Callback(hObject, eventdata, handles)
% hObject    handle to stopSim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in flagSM.
function flagSM_Callback(hObject, eventdata, handles)
% hObject    handle to flagSM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of flagSM


% --- Executes on button press in flagAnim.
function flagAnim_Callback(hObject, eventdata, handles)
% hObject    handle to flagAnim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of flagAnim


% --- Executes on selection change in RWnumber.
function RWnumber_Callback(hObject, eventdata, handles)
% hObject    handle to RWnumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns RWnumber contents as cell array
%        contents{get(hObject,'Value')} returns selected item from RWnumber


% --- Executes during object creation, after setting all properties.
function RWnumber_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RWnumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function RW1_Callback(hObject, eventdata, handles)
% hObject    handle to RW1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of RW1 as text
%        str2double(get(hObject,'String')) returns contents of RW1 as a double


% --- Executes during object creation, after setting all properties.
function RW1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RW1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function RW2_Callback(hObject, eventdata, handles)
% hObject    handle to RW2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of RW2 as text
%        str2double(get(hObject,'String')) returns contents of RW2 as a double


% --- Executes during object creation, after setting all properties.
function RW2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RW2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function RW3_Callback(hObject, eventdata, handles)
% hObject    handle to RW3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of RW3 as text
%        str2double(get(hObject,'String')) returns contents of RW3 as a double


% --- Executes during object creation, after setting all properties.
function RW3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RW3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function RW4_Callback(hObject, eventdata, handles)
% hObject    handle to RW4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of RW4 as text
%        str2double(get(hObject,'String')) returns contents of RW4 as a double


% --- Executes during object creation, after setting all properties.
function RW4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RW4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in RWdir.
function RWdir_Callback(hObject, eventdata, handles)
% hObject    handle to RWdir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns RWdir contents as cell array
%        contents{get(hObject,'Value')} returns selected item from RWdir


% --- Executes during object creation, after setting all properties.
function RWdir_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RWdir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in flag6DOF.
function flag6DOF_Callback(hObject, eventdata, handles)
% hObject    handle to flag6DOF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of flag6DOF


% --- Executes on button press in flagWOL.
function flagWOL_Callback(hObject, eventdata, handles)
% hObject    handle to flagWOL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of flagWOL



function xCOG_Callback(hObject, eventdata, handles)
% hObject    handle to xCOG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xCOG as text
%        str2double(get(hObject,'String')) returns contents of xCOG as a double


% --- Executes during object creation, after setting all properties.
function xCOG_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xCOG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function yCOG_Callback(hObject, eventdata, handles)
% hObject    handle to yCOG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of yCOG as text
%        str2double(get(hObject,'String')) returns contents of yCOG as a double


% --- Executes during object creation, after setting all properties.
function yCOG_CreateFcn(hObject, eventdata, handles)
% hObject    handle to yCOG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function zCOG_Callback(hObject, eventdata, handles)
% hObject    handle to zCOG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of zCOG as text
%        str2double(get(hObject,'String')) returns contents of zCOG as a double


% --- Executes during object creation, after setting all properties.
function zCOG_CreateFcn(hObject, eventdata, handles)
% hObject    handle to zCOG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in flagInertia.
function flagInertia_Callback(hObject, eventdata, handles)
% hObject    handle to flagInertia (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of flagInertia

function simTime_Callback(hObject, eventdata, handles)
% hObject    handle to simTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of simTime as text
%        str2double(get(hObject,'String')) returns contents of simTime as a double


% --- Executes during object creation, after setting all properties.
function simTime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to simTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in flagWOLstop.
function flagWOLstop_Callback(hObject, eventdata, handles)
% hObject    handle to flagWOLstop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of flagWOLstop
