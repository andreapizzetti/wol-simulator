%% ------ AUTONOMOUS WOL - AOCS SIMULATOR ------- %%

% Satellite: M-Argo
% Test-case: 2010UE51 NEA Fuel-Optimal Trajectory 
% Timeframe: 4 february 2024 12:01:09 - 10 february 2024 12:01:09

% NEA                     DEP.TIME   DEP.DATE       TOF  PROP
% 2000 SG344 Baseline     8712       08-Nov-2023    833  1.156
%            Backup       9077       07-Nov-2024    707  1.051
% 2010 UE51  Baseline     8660       17-Sep-2023    581  0.974
%            Backup       9025       16-Sep-2024    861  1.787
% 2011MD     Baseline     8556       05-Jun-2023    854  1.536
%            Backup       8869       13-Apr-2024    742  1.386
% 2012 UV136 Baseline     8712       08-Nov-2023    805  1.895
%            Backup       9077       07-Nov-2024    861  2.313
% 2014 YD    Baseline     8608       27-Jul-2023    721  1.966
%            Backup       8973       26-Jul-2024    651  1.518

clear
clc
close all

addpath data functions animate thesis_plots

if bdIsLoaded('margo')==1
else
margo
end

user_interface