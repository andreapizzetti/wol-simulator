%% MONTECARLO ANALYSIS

clear
clc
close all

addpath 'G:\Il mio Drive\PoliMi\Tesi\07_MODEL\data'
addpath 'G:\Il mio Drive\PoliMi\Tesi\07_MODEL\functions'

set(groot,'defaultAxesFontSize',18, 'defaultAxesTickLabelInterpreter','latex','defaultLegendInterpreter','latex','defaultTextInterpreter','latex');
set(0, 'DefaultLineLineWidth', 2);

load data_3rw

max_sim_time=10;

%% Generate set of samples

% number of samples
n_samples=500;

% saturation level
h_sat=19;
% norm of each h0
h_norm=25;

% initial momentum
[h0_mat] = h0_samples(n_samples,h_sat,h_norm);

% norm of max displacement
COGdisp_norm=12; %[mm]
[COGdisp_mat] = COGdisp_samples(n_samples,COGdisp_norm);

%% Iterate simulations

% reduce the tolerances and set time to 10 h
abs_tol = 1e-8;
rel_tol = 1e-8;
sim_time = max_sim_time*3600;
set_param('margo', 'AbsTol', num2str(abs_tol), 'RelTol', num2str(rel_tol), 'StopTime',num2str(sim_time));

% init variables of interest
hfin=zeros(3,n_samples);
hfin_norm=zeros(1,n_samples);
tfin=zeros(1,n_samples);

for i=1:n_samples
    tic
   
    % assign values
    p.ICs.h0=h0_mat(:,i)*1e-3;
    p.COGdisp=COGdisp_mat(:,i)*1e-3;
    
    % M-Argo Data
    p=satellite(p, flag);
    %  SRP torques for Flywheel and SSP strategies
    p=flywheel_ssa_torques(p);
    
    p.h_limit_end=p.h_limit;
    
    out=sim('margo');
    hfin(:,i)=out.h(end,:);
    hfin_norm(:,i)=1e3*norm(hfin(:,i));
    tfin(:,i)=out.tout(end)/3600;
    if tfin(:,i)==10
        disp('Fail')
    end
    toc
    disp(num2str(i))
end

%% SAVING

save('sensitivity_results_com') 

%% PLOTS

figure()
grid on
hold on
axis equal
view([45+180 20])

indexes=tfin==max_sim_time;
plot3(COGdisp_mat(1,~indexes),COGdisp_mat(2,~indexes),COGdisp_mat(3,~indexes),'o')
plot3(COGdisp_mat(1,indexes),COGdisp_mat(2,indexes),COGdisp_mat(3,indexes),'o')
xlabel('$d_x$ [mm]')
ylabel('$d_y$ [mm]')
zlabel('$d_z$ [mm]')

if sum(indexes)>1
figure()
grid on
hold on
boxplot([COGdisp_mat(1,indexes)',COGdisp_mat(2,indexes)',COGdisp_mat(3,indexes)'],'labels',{'x','y','z'});
end

% success ratio
success=100*(1-sum(indexes)/n_samples)

% 10mm 98.8%
% 12mm
% 14mm
% 16mm


% plot data only of success
tfin(indexes)=[];
hfin_norm(indexes)=[];

% statistics for hfin
mu_hfin=mean(hfin_norm);
sigma_hfin=std(hfin_norm);

% statistics for tfin
mu_tfin=mean(tfin);
sigma_tfin=std(tfin);

% figure of merit Xi=hfin*twol [Nms^2]
eta=1e-3*3600*hfin_norm.*tfin;

figure()
hold on
grid on
plot(tfin, hfin_norm,'o')
plot(tfin,mu_hfin*ones(1,length(hfin_norm)))
plot(mu_tfin*ones(1,length(tfin)),linspace(0.1,0.4,length(tfin)))
xlabel('$t_{f}$ [h]');
ylabel('$h_{f}$ [mNms]');
legend('$h_{f_i}$','$\mu_{h_{f}}$','$\mu_{t_{f}}$')

figure()
subplot(1,2,1)
hold on
grid on
[f,x]=ecdf(tfin);
x_fake=linspace(0,7,1e3);
y=cdf('Normal',x_fake,mu_tfin,sigma_tfin);
plot(x,f)
plot(x_fake,y,'--')
xlabel('$t_{f}$ [h]');
ylabel('$P$ [-]');
subplot(1,2,2)
hold on
grid on
x_fake=linspace(0.1,0.4,1e3);
y=cdf('Normal',x_fake,mu_hfin,sigma_hfin);
[f,x]=ecdf(hfin_norm);
plot(x,f)
plot(x_fake,y,'--')
xlabel('$h_f$ [mNms]');
ylabel('$P$ [-]');

figure()
hold on
grid on
[f,x]=ecdf(eta);
plot(x,f)
xlabel('$\eta$ [Nms$^2$]');
ylabel('$P$ [-]');

