function [h0_mat] = h0_samples(n_samples,h_sat,h_norm)

% init
h0_mat=[];

% root of sum of squares of the other two components (radius of the
% circumference)
s=sqrt(h_norm^2-h_sat^2);

for i=1:n_samples

% init
h0=zeros(3,1);

% index of axes
axes=randperm(3,3);

% random sign
sgn=2*randi([0 1],1)-1;

% set one axis saturated
h_1=h_sat*sgn;

% generate random "angle"
ang=randi([-180 180],1);

% momentum on the other two axis
h_2=s*cos(ang);
h_3=s*sin(ang);

% assign momentum to axes
h0(axes(1))=h_1;
h0(axes(2))=h_2;
h0(axes(3))=h_3;

% check momentum norm
disp(num2str(norm(h0)))

h0_mat=[h0_mat,h0(:,1)];

end

