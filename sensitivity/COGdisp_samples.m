function [COGdisp_mat] = COGdisp_samples(n_samples,r)

% init
COGdisp_mat=[];

for i=1:n_samples

% init
COGdisp=zeros(3,1);

% index of axes
axes=randperm(3,3);

% generate random "angles"
az=randi([-180 180],1);
el=randi([-90 90],1);

% momentum on the other two axis
COGdisp_1=r*cos(el)*cos(az);
COGdisp_2=r*cos(el)*sin(az);
COGdisp_3=r*sin(el);

% assign momentum to axes
COGdisp(axes(1))=COGdisp_1;
COGdisp(axes(2))=COGdisp_2;
COGdisp(axes(3))=COGdisp_3;

% check momentum norm
disp(num2str(norm(COGdisp)))

COGdisp_mat=[COGdisp_mat,COGdisp(:,1)];

end

