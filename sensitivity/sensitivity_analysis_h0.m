%% MONTECARLO ANALYSIS

clear
clc
close all

set(groot,'defaultAxesFontSize',18, 'defaultAxesTickLabelInterpreter','latex','defaultLegendInterpreter','latex','defaultTextInterpreter','latex');
set(0, 'DefaultLineLineWidth', 2);

load sensitivity_data_3w

max_sim_time=10;

%% Generate set of samples

% number of samples
n_samples=500;

% saturation level
h_sat=19;
% norm of each h0
h_norm=25;
[h0_mat] = h0_samples(n_samples,h_sat,h_norm);

%% Iterate simulations

% reduce the tolerances and set time to 10 h
abs_tol = 1e-8;
rel_tol = 1e-8;
sim_time = max_sim_time*3600;
set_param('margo', 'AbsTol', num2str(abs_tol), 'RelTol', num2str(rel_tol), 'StopTime',num2str(sim_time));

% init variables of interest
hfin=zeros(3,n_samples);
hfin_norm=zeros(1,n_samples);
tfin=zeros(1,n_samples);

for i=1:n_samples
    tic

    p.ICs.h0=1e-3*h0_mat(:,i);

    out=sim('margo');
    hfin(:,i)=out.h(end,:);
    hfin_norm(:,i)=1e3*norm(hfin(:,i))
    tfin(:,i)=out.tout(end)/3600
    toc
end


%% PLOTS

save('sensitivity_results_h0') 

figure()
grid on
hold on
axis equal
view([45 20])

plot3(h0_mat(1,:),h0_mat(2,:),h0_mat(3,:),'o')
xlabel('$h_x$ [mNms]')
ylabel('$h_y$ [mNms]')
zlabel('$h_z$ [mNms]')

% statistics for hfin
mu_hfin=mean(hfin_norm);
sigma_hfin=std(hfin_norm);

% statistics for tfin
mu_tfin=mean(tfin);
sigma_tfin=std(tfin);

% figure of merit Xi=hfin*twol [Nms^2]
eta=1e-3*3600*hfin_norm.*tfin;

figure()
hold on
grid on
plot(tfin, hfin_norm,'o')
plot(tfin,mu_hfin*ones(1,length(hfin_norm)))
plot(mu_tfin*ones(1,length(tfin)),linspace(0.1,0.4,length(tfin)))
xlabel('$t_{f}$ [h]');
ylabel('$h_{f}$ [mNms]');
legend('$h_{f_i}$','$\mu_{h_{f}}$','$\mu_{t_{f}}$')

figure()
subplot(1,2,1)
hold on
grid on
[f,x]=ecdf(tfin);
plot(x,f)
xlabel('$t_{f}$ [h]');
ylabel('$P$ [-]');
subplot(1,2,2)
hold on
grid on
[f,x]=ecdf(hfin_norm);
plot(x,f)
xlabel('$h_f$ [mNms]');
ylabel('$P$ [-]');

figure()
hold on
grid on
[f,x]=ecdf(eta);
plot(x,f)
xlabel('$\eta$ [Nms$^2$]');
ylabel('$P$ [-]');

