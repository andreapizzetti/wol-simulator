function [R_RW,Rstar_RW]=rwparam(p,flag)

if flag.RW==1
% 3-axis configuration
    n_RW=[1 0 0;
          0 1 0;
          0 0 1];
end

if flag.RW==2
% pyramid configuration
    switch p.RWdir
        case 1  %x
        n_RW=[1  1  1;
              1 -1  1;
              1  1 -1;
              1 -1 -1]; 
        case 4  %-x
        n_RW=[-1  1  1;
              -1 -1  1;
              -1  1 -1;
              -1 -1 -1]; 
        case 2  %y
        n_RW=[1  1  1;
              1  1 -1;
             -1  1  1;
             -1  1 -1]; 
        case 5  %-y
        n_RW=[1 -1  1;
              1 -1 -1;
             -1 -1  1;
             -1 -1 -1]; 
        case 3  %z
        n_RW=[1  1  1;
              1 -1  1;
             -1  1  1;
             -1 -1  1]; 
        case 6  %-z
        n_RW=[1  1 -1;
              1 -1 -1;
             -1  1 -1;
             -1 -1 -1]; 
    end
end

for i=1:size(n_RW,1)
    n(i,:)=n_RW(i,:)/norm(n_RW(i,:));
    R_RW(:,i)=n(i,:);
end

Rstar_RW=R_RW'*(inv(R_RW*R_RW'));

end