function run_simulation(input)

%% RETRIEVE INPUT FROM GUI
[p,flag]=elaborate_input(input);

%% FLAGS
flag.LQR=0;             % 0 - Tracking
                        % 1 - LQR
flag.movie=0;
flag.debug=0;

%% CONSTANTS
p.AU=1.49597871e8; %[km]
p.P0=4.539807335646850e-06; %[Pa]

%% STATE MACHINE
% SM angular momentum limit
if flag.cruising==1
    if flag.LQR==1
    p.h_limit=5e-4; % for gimbal thruster strategies  LQR
    p.h_limit_end=1e-4; % end procedure
    p.err_limit_0=5e-4;
    p.err_limit_1=1e-1;
    p.err_limit_end=1e-3;
    else
p.h_limit=3e-4;   % for gimbal thruster strategies  Tracking
p.h_limit_end=1e-4; % end procedure
p.err_limit_0=4e-3;
p.err_limit_1=4e-3;
p.err_limit_end=1e-3;
    end
end
if flag.cruising==0
    if flag.LQR==1
p.h_limit=5e-4;   % for arrays strategies LQR
p.h_limit_end=1e-4; % end procedure
p.err_limit_0=1e-3;
p.err_limit_1=1e-1;
p.err_limit_end=1e-3;
    else
p.h_limit=3e-4;   % for arrays strategies Tracking Controller
p.h_limit_end=1e-4; % end procedure
p.err_limit_0=2e-2;
p.err_limit_1=2e-2;
p.err_limit_end=1e-3;
    end
end

%% WOL STRATEGIES PARAMETERS
% version of sadm logic
p.v_SADM=2; %found to be the most efficient one

if flag.cruising==1
    % for gimbal strategies
    p.ph_max=5*pi/180;  % maximum attitude angles
    p.th_max=5*pi/180; % maximum gimbal angles
    p.T=20*60;          % frequency of BETA and SRPW trajectories
else
    % for arrays strategies
    p.ph_max=20*pi/180;
    p.th_max=5*pi/180;
    p.T=2*60*60;   
end
   
p.om_att=2*pi*1/p.T;   
p.om_sa=2*pi*1/p.T;     
p.om_thr=2*pi*1/p.T;     

%% DATA
% Reference Trajectory Data
p=reference(p,flag);
% M-Argo Data
p=satellite(p, flag);
% ICs & Guidance
[p, flag]=initial(p,flag);
% Control
p=controller(p, flag);
%  SRP torques for Flywheel and SSP strategies
p=pinwheel_ssa_torques(p);

%% SIMULATION
sim_time = p.sim_time_hours*3600;  
max_dt = 1; %[s]
abs_tol = 1e-10;
rel_tol = 1e-10;
solver_name = 'ode45';
solver_type = 'variable-step';

set_param('margo', 'Solver', solver_name,...
    'SolverType', solver_type, ....
    'MaxStep', num2str(max_dt), 'AbsTol',...
    num2str(abs_tol), 'RelTol', num2str(rel_tol),...
    'StopTime',num2str(sim_time));

options = simset('SrcWorkspace','current');

out=sim('margo',[],options);
save('results');

if flag.plot6DOF==1
flag.WOL=0;
sim_time=out.tout(end);
set_param('margo','StopTime',num2str(sim_time));
out_nowol=sim('margo',[],options);
save('results_nowol','out_nowol');
end

%% RESULTS
set(groot,'defaultAxesFontSize',18, 'defaultAxesTickLabelInterpreter','latex','defaultLegendInterpreter','latex','defaultTextInterpreter','latex');
set(0, 'DefaultLineLineWidth', 2);

out=plots(p,out,flag);


%% ANIMATION

% for animation
movie.speed=2+0.5e-1*floor(out.tout(end)/3600);
movie.title='WOL';

if flag.animation==1
animate(out,flag,movie)
end
