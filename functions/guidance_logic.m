function [p,flag]=guidance_logic(p,flag)

c=@(x) cos(x);
s=@(x) sin(x);

%% CHOICE OF STRATEGY (GIMBAL AND SADM ANGLES)
% flag.thrust=0;          % 0 - zero thrust
%                         % 1 - nominal thrust
% flag.attitudeTraj=2;    % 0 - fixed attitude
%                         % 1 - wol around alpha with gimbal mechanism
%                         % 2 - wol around beta with sadm mechanism
% flag.reverse=1          % 1 - normal trajectory
%                         %-1 - reversed trajectory 

%---INIT---%
p.th1_max=0;
p.th2_max=0;
p.d1_max=0;
p.d2_max=0;

h0=p.sat.R_RW*p.ICs.h0; %equivalent angular momentum
hx=h0(1);
hy=h0(2);
hz=h0(3);

if flag.cruising==0
    % SOLAR PANELS STRATEGIES
    flag.thrust=0;

    if abs(hy) >= abs(hx) && abs(hy) >= abs(hz)
        % SRP Wading trajectory
        flag.attitudeTraj=2;
        flag.reverse=sign(hy); %clockwise/anticlockwise
        p.d1_max=90*pi/180;
        p.d2_max=90*pi/180;
    else
        % Flywheel o Single-SP trajectories (depending on sun direction)
        flag.attitudeTraj=0;
        flag.reverse=1;
        S_p=-p.ICs.A_np0'*p.ICs.r0/norm(p.ICs.r0);
        h_eq=p.sat.R_RW*p.ICs.h0;
        [p.d1_max,p.d2_max]=delta_sp(h_eq(1),h_eq(3),S_p(1),S_p(3));
    end
end

if flag.cruising==1
    % GIMBALLED THRUSTER STRATEGIES
    flag.thrust=1;
    thmax=p.th_max;

    if abs(hz) >= abs(hx) && abs(hz) >= abs(hy)
        % Bi-Elicoidal Thruster-Attitude trajectory (BETA trajectory)
        flag.attitudeTraj=1;
        flag.reverse=sign(hz);
        p.th1_max=thmax*sign(hz);
        p.th2_max=thmax;

    elseif abs(hx)>=abs(hy)
        % Gimballed Thruster trajectory 
        flag.attitudeTraj=0;
        flag.reverse=1;
        p.th2_max=-thmax*sign(hx);
        p.th1_max=asin(abs(sin(p.th2_max)*hy/hx))*sign(hy);

    else
        % Gimballed Thruster trajectory 
        flag.attitudeTraj=0;
        flag.reverse=1;
        p.th1_max=thmax*sign(hy);
        p.th2_max=-asin(abs(sin(p.th1_max)*hx/hy))*sign(hx);    
    end

end

if flag.cruising==2
    % MIXED STRATEGIES
    flag.thrust=1;
    thmax=p.th_max;

    if abs(hz) >= abs(hx) && abs(hz) >= abs(hy)
        % BETA + Single SP trajectory
        flag.attitudeTraj=1;
        flag.reverse=sign(hz);
        p.th1_max=thmax*sign(hz);
        p.th2_max=thmax;
        p.d1_max=-35*pi/180;
        p.d2_max=35*pi/180;

    else
        % Gimballed Thruster + Flywheel trajectory
        flag.attitudeTraj=0;
        flag.reverse=1;
        p.th2_max=-thmax*sign(hx);
        p.th1_max=asin(abs(sin(p.th2_max)*hy/hx))*sign(hy);
        p.d1_max=35*pi/180;
        p.d2_max=-35*pi/180;  
    end

end

%% INITIAL ATTITUDE OF BODY WRT POINTING

%---INIT---%
p.ICs.A_pb0=eye(3);

if flag.statemachine==0 
    if flag.attitudeTraj==1
    ph1=0;
    ph2=p.ph_max;
    p.ICs.A_pb0=[c(ph1)  0       s(ph1);
                 0       c(ph2)  s(ph2);
                -s(ph1) -s(ph2)  sqrt(1-s(ph1).^2-s(ph2).^2)];
    elseif flag.attitudeTraj==2
    ph1=0;
    ph2=p.ph_max;
    p.ICs.A_pb0=[c(ph2)    -s(ph2) 							  0;
                 s(ph2)     sqrt(1-s(ph1)^2-s(ph2)^2)         s(ph1);
                 0	       -s(ph1)		  					  c(ph1)];
    end
end
