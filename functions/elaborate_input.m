function [p,flag]=elaborate_input(input)

flag.animation=input.flagAnim;
flag.statemachine=input.flagSM;
flag.plot6DOF=input.flag6DOF;
flag.WOL=input.flagWOL;
flag.inertia_variation=input.flagInertia;
flag.WOLstop=input.flagWOLstop;

if input.scenario=="Cruising"
    flag.cruising=1;
elseif input.scenario=="Coasting"
    flag.cruising=0;
end

if input.RWnumber==2
    flag.RW=2;
    p.ICs.h0=[str2num(input.h1);
              str2num(input.h2);
              str2num(input.h3);
              str2num(input.h4)];
end
if input.RWnumber==1
    flag.RW=1;
    p.ICs.h0=[str2num(input.h1);
              str2num(input.h2);
              str2num(input.h3)];
end
p.RWdir=input.RWdir;

p.COGdisp=[str2num(input.xCOG);
           str2num(input.yCOG);
           str2num(input.zCOG)]*1e-3;  %[m]

p.sim_time_hours=str2num(input.simTime);    %[h]
end