function p=pinwheel_ssa_torques(p)

% initial position
r=p.ICs.r0;

% sun direction in body frame
s_n=-r/norm(r);
A_pn=p.ICs.A_np0';
A_bp=eye(3);
A_bn=A_bp*A_pn;
S=A_bn*s_n;

% solar pressure @ 1 AU
P=p.P0/((norm(r)/p.AU)^2);

% offset angle
d_offset=acos(dot(p.ICs.alpha0,s_n));
fw_angle=35;
d1_pinwheel=d_offset+fw_angle*pi/180;
d2_pinwheel=d_offset-fw_angle*pi/180;
d1_ssp=d_offset;
d2_ssp=d_offset+90*pi/180;

% retrieve geometry data
A_vec=p.sat.A;
r_mat=p.sat.r;
N_mat_pinwheel=p.sat.N(d1_pinwheel,d2_pinwheel);
N_mat_ssp=p.sat.N(d1_ssp,d2_ssp);
rho_diff=p.sat.rho_diff;
rho_spec=p.sat.rho_spec;

% number of surfaces
n=length(A_vec);   

% init
T_pinwheel=zeros(3,1);
T_ssa=zeros(3,1);

for i=1:n
    N=N_mat_pinwheel(:,i);
    SN=dot(S,N);
    if SN>0
    A=A_vec(i);
    r=r_mat(:,i);
    rs=rho_spec(i);
    rd=rho_diff(i);
        F=-P*A*(SN)*((1-rs)*S+(2*rs*(SN)+2/3*rd)*N);
        T_temp=cross(r,F);
    else
        T_temp=[0; 0; 0];
    end
    T_pinwheel=T_pinwheel+T_temp;
end
T_pinwheel=abs(T_pinwheel);

for i=1:n
    N=N_mat_ssp(:,i);
    SN=dot(S,N);
    if SN>0
    A=A_vec(i);
    r=r_mat(:,i);
    rs=rho_spec(i);
    rd=rho_diff(i);
        F=-P*A*(SN)*((1-rs)*S+(2*rs*(SN)+2/3*rd)*N);
        T_temp=cross(r,F);
    else
        T_temp=[0; 0; 0];
    end
    T_ssa=T_ssa+T_temp;
end
T_ssa=abs(T_ssa);

p.T_pinwheel=T_pinwheel;
p.T_ssa=T_ssa;
end