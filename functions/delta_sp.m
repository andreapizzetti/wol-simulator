function [d1_max,d2_max]=delta_sp(h_eq_x,h_eq_z,S_p_delta,S_p_alpha)

aVarTruthTableCondition_1 = false;
aVarTruthTableCondition_2 = false;
aVarTruthTableCondition_3 = false;
aVarTruthTableCondition_4 = false;


% SUN_DELTA

aVarTruthTableCondition_1 = logical(S_p_delta>0);

% SUN_ALPHA

aVarTruthTableCondition_2 = logical(S_p_alpha>0);

% H_EQ_X

aVarTruthTableCondition_3 = logical(h_eq_x>0);

% H_EQ_Z

aVarTruthTableCondition_4 = logical(h_eq_z>0);

if (aVarTruthTableCondition_1 && aVarTruthTableCondition_2 && aVarTruthTableCondition_3 && aVarTruthTableCondition_4)
    c=4;
elseif (~aVarTruthTableCondition_1 && aVarTruthTableCondition_2 && aVarTruthTableCondition_3 && aVarTruthTableCondition_4)
    c=1;
elseif (aVarTruthTableCondition_1 && ~aVarTruthTableCondition_2 && aVarTruthTableCondition_3 && aVarTruthTableCondition_4)
    c=2;
elseif (aVarTruthTableCondition_1 && aVarTruthTableCondition_2 && ~aVarTruthTableCondition_3 && aVarTruthTableCondition_4)
    c=1;
elseif (aVarTruthTableCondition_1 && aVarTruthTableCondition_2 && aVarTruthTableCondition_3 && ~aVarTruthTableCondition_4)
    c=2;
elseif (~aVarTruthTableCondition_1 && ~aVarTruthTableCondition_2 && aVarTruthTableCondition_3 && aVarTruthTableCondition_4)
    c=3;
elseif (aVarTruthTableCondition_1 && ~aVarTruthTableCondition_2 && ~aVarTruthTableCondition_3 && aVarTruthTableCondition_4)
    c=4;
elseif (aVarTruthTableCondition_1 && aVarTruthTableCondition_2 && ~aVarTruthTableCondition_3 && ~aVarTruthTableCondition_4)
    c=3;
elseif (~aVarTruthTableCondition_1 && aVarTruthTableCondition_2 && aVarTruthTableCondition_3 && ~aVarTruthTableCondition_4)
    c=4;
elseif (~aVarTruthTableCondition_1 && ~aVarTruthTableCondition_2 && ~aVarTruthTableCondition_3 && aVarTruthTableCondition_4)
    c=1;
elseif (aVarTruthTableCondition_1 && ~aVarTruthTableCondition_2 && ~aVarTruthTableCondition_3 && ~aVarTruthTableCondition_4)
    c=1;
elseif (~aVarTruthTableCondition_1 && aVarTruthTableCondition_2 && ~aVarTruthTableCondition_3 && ~aVarTruthTableCondition_4)
    c=2;
elseif (~aVarTruthTableCondition_1 && ~aVarTruthTableCondition_2 && aVarTruthTableCondition_3 && ~aVarTruthTableCondition_4)
    c=2;
elseif (~aVarTruthTableCondition_1 && aVarTruthTableCondition_2 && ~aVarTruthTableCondition_3 && aVarTruthTableCondition_4)
    c=3;
elseif (aVarTruthTableCondition_1 && ~aVarTruthTableCondition_2 && aVarTruthTableCondition_3 && ~aVarTruthTableCondition_4)
    c=3;
else % Default
    c=4;
end
check=1;
switch check
    case c==1
% SSP-1
d1_max=0;
d2_max=90*pi/180;
    case c==2
% SSP-2
d1_max=90*pi/180;
d2_max=0;
    case c==3
% FW-1
d1_max=-35*pi/180;
d2_max=35*pi/180;
    case c==4
% FW-2
d1_max=35*pi/180;
d2_max=-35*pi/180;
end

end
