function [J, Jinv]= inertia(d1, d2, m, sat)
% Precise computation of inertia given mass and solar arrays deflection angles
% Hypothesis: homogenous distribution of mass

a=sat.a;
b=sat.b;
c=sat.c;
h_sa=sat.h_sa;
l_sa=sat.l_sa;
l_h=sat.l_h;
m_sa=sat.m_sa;

%% BODY
% extract only mass of the body
m_body=m-2*m_sa;
% Inertia of cube
J_body=m_body/12*[b^2+c^2     0           0;
                  0           a^2+c^2     0
                  0           0           a^2+b^2];

%% SOLAR ARRAYS
% Inertia of solar array in his CG
J_sa=@(d) m_sa*h_sa^2/12*[(l_sa/h_sa)^2+sin(d)^2    0               0;
                           0                        1               -sin(d)*cos(d);
                           0                       -sin(d)*cos(d)   (l_sa/h_sa)^2+cos(d)^2];
  
% Inertia of solar array in CubeSat CG   
J_sa_body=@(R, d) J_sa(d)+m_sa*(dot(R,R)*eye(3)-[R(1)^2     R(1)*R(2)   R(1)*R(3);
                                                 R(2)*R(1)  R(2)^2      R(2)*R(3);
                                                 R(3)*R(1)  R(3)*R(2)   R(3)^2]);

R_sa1=[0,-(b/2+l_h+l_sa/2),0];
R_sa2=-R_sa1;

J_sa1_body=J_sa_body(R_sa1,d1);
J_sa2_body=J_sa_body(R_sa2,d2);

% Total Inertia
J=J_body+J_sa1_body+J_sa2_body;
% Jinv=[1/J(1,1)  0           0;
%       0         1/J(2,2)    0;
%       0         0           1/J(3,3)];
Jinv=inv(J);
end
