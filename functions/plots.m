function out=plots(p,out,flag)

close all
t=out.tout/3600;    %hours

figure()
hold on
grid on
plot(t,out.w*180/pi)
xlabel('$t$ [h]')
ylabel('$\omega$ [$^\circ$/s]')
legend('$\omega_x$','$\omega_y$','$\omega_z$') 

% figure()
% hold on
% grid on
% plot(out.tout,out.w_des*180/pi)
% xlabel('t [s]')
% ylabel('$\omega_{des}$ [deg/s]')

if flag.cruising==0
figure()
hold on
grid on
% plot(t,out.T_srp*1e3)
plot(t,out.T_srp_p*1e3)
xlabel('$t$ [h]')
ylabel('$T$ [mNm]')
% legend('$T_{srp_x}$','$T_{srp_y}$','$T_{srp_z}$')
legend('$T_{srp_\delta}$','$T_{srp_\beta}$','$T_{srp_\alpha}$')
else
figure()
hold on
grid on
% plot(t,out.T_thr*1e3)
plot(t,out.T_thr_p*1e3)
xlabel('$t$ [h]')
ylabel('$T$ [mNm]')
%legend('$T_{thr_x}$','$T_{thr_y}$','$T_{thr_z}$')
legend('$T_{thr_\delta}$','$T_{thr_\beta}$','$T_{thr_\alpha}$')
end

figure()
hold on
grid on
plot(t,out.point_err_vec)
plot(t,out.point_err,'--','LineWidth',2)
xlabel('$t$ [h]')
ylabel('$\epsilon$ [$rad$]')
legend('$\epsilon_x$','$\epsilon_y$','$\epsilon_z$','$||\epsilon||$')

figure()
hold on
grid on
plot(t,out.T_c_des*1e3)
% plot(t,out.T_c*1e3)
xlabel('$t$ [h]')
ylabel('$T$ [mNm]')
legend('$T_{com_x}$','$T_{com_y}$','$T_{com_z}$')
% legend('$T_{com_x}$','$T_{com_y}$','$T_{com_z}$','$T_{eff_x}$','$T_{eff_y}$','$T_{eff_z}$')

figure()
hold on
grid on
if flag.cruising==1
subplot(2,1,1)
hold on
grid on
plot(t,out.th1*180/pi)
plot(t,out.th2*180/pi)
xlabel('$t$ [h]')
ylabel('$\theta$ [$^\circ$]')
legend('$\theta_1$','$\theta_2$')
subplot(2,1,2)
hold on
grid on
plot(t,out.ph1*180/pi)
plot(t,out.ph2*180/pi)
xlabel('$t$ [h]')
ylabel('$\phi$ [$^\circ$]')
legend('$\phi_1$','$\phi_2$');
elseif flag.cruising==0
subplot(2,1,1)
hold on
grid on
plot(t,out.d1_rel*180/pi)
plot(t,out.d2_rel*180/pi)
xlabel('$t$ [h]')
ylabel('$\Delta_{rel}$ [$^\circ$]')
legend('$\Delta_{rel_1}$','$\Delta_{rel_2}$')
subplot(2,1,2)
hold on
grid on
plot(t,out.ph1*180/pi)
plot(t,out.ph2*180/pi)
xlabel('$t$ [h]')
ylabel('$\phi$ [$^\circ$]')
legend('$\phi_1$','$\phi_2$');
end

% figure()
% hold on
% grid on
% plot(t,out.d_offset*180/pi)
% xlabel('$t$ [h]')
% ylabel('$\Delta_{\odot}$ [$^\circ$]')

if flag.thrust==1
figure()
hold on
grid on
plot(t,out.F_max)
plot(t,out.F_av)
plot(t,out.F_ref*1e3)
% 0 to nan in commanded thrust
F_com=out.F_com;
F_com(F_com==0)=nan;
plot(t,F_com*1e3,'LineWidth',2)
xlabel('$t$ [h]')
ylabel('$F$ [mN]')
ylim([0.9 1.5])
legend('$F_{max}$','$F_{av}$','$F_{ref}$','$F_{com}$')

figure()
hold on
grid on
plot(t,out.Isp)
plot(t,out.Isp_max)
xlabel('$t$ [h]')
ylabel('$I_{sp}$ [s]')
legend('$I_{sp}$',' $I_{sp_{max}}$')
else
    figure()
    figure()
end
figure()
hold on
grid on
plot(t,out.P_max)
plot(t,out.P_av)
xlabel('$t$ [h]')
ylabel('$P$ [W]')
legend('$P_{max}$','$P_{av}$')
    
[out.pitch, out.roll, out.yaw] = dcm2angle(out.A_bp,'YXZ','Default','None',0.1);
% figure()
% hold on
% grid on
% plot(out.tout,out.yaw)
% plot(out.tout,out.pitch)
% plot(out.tout,out.roll)
% xlabel('t [s]')
% ylabel('angle [deg]')
% legend('yaw','pitch','roll')

%% ANGULAR MOMENTUM
if flag.RW==1
figure()
hold on
grid on
plot(t,out.h/1e-3)
% plot(t,vecnorm(out.h')'/1e-3,'--','LineWidth',2)
legtxt={'$h_{RW_1}$','$h_{RW_2}$','$h_{RW_3}$','$||h_{RW}||$'};

legtxt={'$h_{x}$','$h_{y}$','$h_{z}$'};
legend(legtxt)
xlabel('$t$ [h]')
ylabel('$h$ [mNms]')
end

if flag.RW==2
figure()
subplot(2,1,1)
hold on
grid on
plot(t,out.h/1e-3)
plot(t,vecnorm(out.h')'/1e-3,'--','LineWidth',2)
legtxt={'$h_{RW_1}$','$h_{RW_2}$','$h_{RW_3}$','$h_{RW_4}$','$||h_{RW}||$'};
legend(legtxt)
xlabel('$t$ [h]')
ylabel('$h$ [mNms]')
subplot(2,1,2)
hold on
grid on
plot(t(2:end),out.h_eq(2:end,:)/1e-3)
plot(t(2:end),vecnorm(out.h_eq(2:end,:)')'/1e-3,'--','LineWidth',2)
legend('$h_{eq_{x}}$','$h_{eq_{y}}$','$h_{eq_{z}}$','$||h_{eq}||$')
xlabel('$t$ [h]')
ylabel('$h$ [mNms]')
    
end

%% STATE MACHINE
if flag.statemachine==1
    
states = {'';'Re-Pointing '; 'Gimbal/SSA/FW '; 'BETA '; 'SRPW '; 'WOL Singularity ';'De-Tumbling ';'No WOL ';''};


figure()
hold on
grid on
plot(t,out.state)
ax1 = gca;
%// fontsize of y-axis, deactivate, x-axis
set(ax1,'YTick',-2:6,'YTickLabel',states)
% set(ax1,'XTick',[],'YTick',-2:6,'YTickLabel',states,'FontName','Serif','FontSize',12)
%// create second identical axis and link it to first one
% ax2 = axes('Position', get(ax1, 'Position'),'Color','none');
% linkaxes([ax1,ax2],'xy')
%// fontsize of x-axis, deactivate, y-axis
% set(ax2,'YTick',[])
xlabel('$t$ [h]')

end

%% 6DOF
if flag.plot6DOF==1
    
    load('results_nowol.mat');
    figure()
    hold on
    grid on
    grid minor
    axis equal
     plot3(out.r_eph(:,1),out.r_eph(:,2),out.r_eph(:,3),'k')
    plot3(out.r_n(:,1),out.r_n(:,2),out.r_n(:,3),'g')
    plot3(out_nowol.r_n(:,1),out_nowol.r_n(:,2),out_nowol.r_n(:,3),'r')
    plot3(out.r_n(1,1),out.r_n(1,2),out.r_n(1,3),'bo')
    plot3(out.r_n(end,1),out.r_n(end,2),out.r_n(end,3),'ko')
    xlabel('x [km]')
    ylabel('y [km]')
    zlabel('z [km]')
     legend('$Ref$','$WOL$','$No\,WOL$','$Start$','$End$')
%    legend('$WOL$','$No\,WOL$','$Start$','$End$')
    %title(['t = ',num2str(out.tout(end)/3600),' h']);
    disp(['Ref vs Real error = ', num2str(1e3*norm(out.r_eph(end,:)-out.r_n(end,:))),' m']);
    disp(['Nowol vs Real error = ', num2str(1e3*norm(out_nowol.r_n(end,:)-out.r_n(end,:))),' m']);
    disp(['Ref vs Nowol error = ', num2str(1e3*norm(out.r_eph(end,:)-out_nowol.r_n(end,:))),' m']);

end

%% MASS & INERTIA
figure()
hold on
grid on
mp_ref=1e3*(out.m_ref-p.sat.m_dry_tot);
mp=1e3*(out.m_sc-p.sat.m_dry_tot);
mp_RCS=1e3*(out.m_RCScase-p.sat.m_dry_tot);
mp_cons=mp(1)-mp(end);
mp_cons_ref=mp_ref(1)-mp_ref(end);
mp_cons_RCS=mp_RCS(1)-mp_RCS(end);
mp_saved=mp_cons_RCS-mp_cons;
mp_cons_for_wol=mp_cons-mp_cons_ref;
mp_cons_for_wol_RCS=mp_cons_RCS-mp_cons_ref;

plot(t,mp_ref)
plot(t,mp)
plot(t,mp_RCS)
xlabel('$t$ [h]')
ylabel('$m$ [g]')
legend('$m_{p_{ref}}$','$m_p$ (WOL)','$m_p$ (RCS)')
axes('Position',[0.677083333333333,0.531671858774662,0.166145833333333,0.199065420560746])
box on
hold on
grid on
plot(t,mp_ref)
plot(t,mp)
plot(t,mp_RCS)
xlim([1.8,2.3])
ylim([739.03,739.06])

if flag.inertia_variation==1
figure()
% subplot(2,1,1)
hold on
grid on
Jxx=squeeze(out.J(1,1,:));
Jzz=squeeze(out.J(3,3,:));
Jyz=squeeze(out.J(2,3,:));
Jzy=squeeze(out.J(3,2,:));
% plot(t,reshape(out.J,9,length(out.tout)))
% plot(t,Jyy)
plot(t,Jyz)
xlabel('$t$ [h]')
ylabel('$J$ [kg m$^2$]')
legend('$J_{yz}=J_{zy}$')

% subplot(2,1,2)
% hold on
% grid on
% plot(t,Jxx)
% plot(t,Jzz)
% xlabel('$t$ [h]')
% ylabel('$J$ [kg m$^2$]')
% legend('$J_{xx}$','$J_{zz}$')
end

disp(['-------------RESULTS---------------']);
disp(['Simulation Time = ', num2str(out.tout(end)/3600),' h']);
state5_times=out.tout(out.state==5);
if isempty(state5_times)~=1
disp(['WOL Time = ', num2str(state5_times(1)/3600),' h']);
end
disp(['Residual Momentum = ', num2str(1e3*norm(out.h_eq(end,:))),' mNms']);
disp(['Residual Momentum = ', num2str(100*norm(out.h_eq(end,:))/norm(out.h_eq(2,:))),' %']);
disp(['Propellant consumed (ref) = ', num2str(mp_cons_ref),' g']);
disp(['Propellant consumed = ', num2str(mp_cons),' g']);
disp(['Additional propellant consumed for wol = ', num2str(mp_cons_for_wol),' g']);
disp(['Propellant consumed (RCS case) = ', num2str(mp_cons_RCS),' g']);
disp(['Additional propellant consumed for wol (RCS case = ', num2str(mp_cons_for_wol_RCS),' g']);
disp(['Mass savings = ', num2str(mp_saved),' g']);
disp(['Mass savings = ', num2str(100*(mp_saved/mp_cons_for_wol_RCS)),' %']);

disp(['Energy produced= ', num2str(100*(out.E_av(end)/out.E_max(end))),' %']);
h0=num2str(p.ICs.h0*1e3)
end
